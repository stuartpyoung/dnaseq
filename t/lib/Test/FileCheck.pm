use Moose::Util::TypeConstraints;
use MooseX::Declare;
use Method::Signatures::Modifiers;


class Test::FileCheck extends FileCheck {

use FindBin qw($Bin);
use Test::More;

#####/////}}}}}

method BUILD ($args) {
	if ( defined $args ) {
		foreach my $arg ( $args ) {
			$self->$arg($args->{$arg}) if $self->can($arg);
		}
	}
	 $self->logDebug("args", $args);
}

=doc

=head2	TEST	check

=head2 	PURPOSE

Run the following tests:
 
1. File exists and size correct
2. Filesize is below min
3. Filesize is above max
4. File is link
5. File is directory
6. File input is incorrect (missing fields)

=cut

method testCheck {
	diag("check");
	
	
	my $tests = [
		{
			name			=>	"check",
			args			=> 	{
				inputfile		=>	"$Bin/inputs/filecheck.tsv",
				outputfile		=>	"$Bin/outputs/filecheck.tsv"
			},
			expectedfile	=>	"$Bin/inputs/expected.tsv"
		}
	];
	
	no warnings;
	*bail = sub {
		print "OVERRIDING bail\n";
	};
	use warnings;
	
	foreach my $test (  @$tests ) {
		my $name 		= 	$test->{name};
		my $args 		= 	$test->{args};
		my $inputfile	=	$args->{inputfile};
		my $outputfile	=	$args->{outputfile};
		my $expectedfile=	$test->{expectedfile};

		$self->logDebug("inputfile", $inputfile);
		$self->logDebug("outputfile", $outputfile);
		$self->check($inputfile, $outputfile);
		
		##### COPY TO outputs IF NO outputfile
		if ( not defined $outputfile ) {
			$inputfile = $args->{inputfile};
			$outputfile = $inputfile;
			$outputfile =~ s/inputs/outputs/;
			cp $inputfile $outputfile;
			$args->{inputfile} = $outputfile;			
		}
		
		#$self->logDebug("name",  $name);
		#$self->logDebug("args",  $args);
		
		$self->check($inputfile, $outputfile);
		
		my $diff = $self->diff( $outputfile,  $expectedfile);
		$self->logDebug("diff", $diff);
		
		ok($diff, $name);
	}
}

method diff ($actual, $expected) {
	$self->logDebug("actual", $actual);
	$self->logDebug("expected", $expected);
	
	#### CHECK INPUTS
	$self->logNote("actual not defined") and return 0 if not defined $actual;
	$self->logNote("expected not defined") and return 0 if not defined $expected;
	
	my $diff;
	if ( -d $actual ) {
		$diff	=	`diff -r $actual $expected`;
	}
	else {
		$self->logNote("actual not found") and return 0 if not -f $actual;
		$self->logNote("expected not defined") and return 0 if not -f $expected;
		$diff = `diff -wb $actual $expected`;
	}
	$self->logDebug("diff", $diff);
	
	return 1 if $diff eq "";
	return 0;
}




}	

