use MooseX::Declare;

class Test::Logic::Fork::hethom with (Test::Agua::Common, Test::Agua::Common::Util, Agua::Common) extends Logic::Fork {

use Test::More;
use FindBin qw($Bin);

# Ints
has 'log'=>  ( isa => 'Int', is => 'rw', default => 4 );
has 'printlog'=>  ( isa => 'Int', is => 'rw' );

# STRINGS
has 'dumpfile'=> ( isa => 'Str|Undef', is => 'rw', default => '' );

# OBJECTS
has 'db'=> ( isa => 'Agua::DBase::MySQL', is => 'rw', required => 0 );
has 'conf' => (
is =>'rw',
'isa' => 'Conf::Yaml',
default=>sub { Conf::Yaml->new( backup=>1 );});

method testPrintOutputfile {
	diag("printOutputfile");

	$self->logDebug("");

	my $tests = [
	    {
                name        =>  "pass",
                inputfiles  =>  "$Bin/inputs/hethom-pass,$Bin/inputs/parameters.txt",
                modfile     =>  "$Bin/../../../../../lib/Logic/Fork/HetHom.pm",
                expectedfile=>  "$Bin/inputs/hethom-pass.tsv",
				outputfile	=>  "$Bin/outputs/hethom-pass.tsv"
	    },	    
	    {
                name        =>  "warning",
                inputfiles  =>  "$Bin/inputs/hethom-warning,$Bin/inputs/parameters.txt",
                modfile     =>  "$Bin/../../../../../lib/Logic/Fork/HetHom.pm",
                expectedfile=>  "$Bin/inputs/hethom-warning.tsv",
				outputfile	=>  "$Bin/outputs/hethom-warning.tsv"
	    },	    
	    {
                name        =>  "failure",
                inputfiles  =>  "$Bin/inputs/hethom-failure,$Bin/inputs/parameters.txt",
                modfile     =>  "$Bin/../../../../../lib/Logic/Fork/HetHom.pm",
                expectedfile=>  "$Bin/inputs/hethom-failure.tsv",
				outputfile	=>  "$Bin/outputs/hethom-failure.tsv"
	    }	    
    ];
    foreach my $test (@$tests) {
		my $name            =   $test->{name};
		my $inputfiles      =   $test->{inputfiles};
		my $modfile         =   $test->{modfile};
		my $expectedfile    =   $test->{expectedfile};
		my $outputfile      =   $test->{outputfile};
		$self->logDebug("inputfiles", $inputfiles);
		$self->logDebug("modfile", $modfile);
		$self->logDebug("expectedfile", $expectedfile);
		$self->loadModfile($modfile);

		my ($index, $results) = $self->selectIndex($inputfiles);
		$self->logDebug("results", $results);
	
		$self->printOutputfile($outputfile, $results);
		
		ok($self->diff($expectedfile, $outputfile), $name);
	}
	$self->logDebug("completed");
}

method testSelectIndex {
	diag("selectIndex");

	$self->logDebug("");

	my $tests = [
	    {
                name        =>  "pass",
                inputfiles  =>  "$Bin/inputs/hethom-pass,$Bin/inputs/parameters.txt",
                modfile     =>  "$Bin/../../../../../lib/Logic/Fork/HetHom.pm",
                expectedfile=>  "$Bin/inputs/hethom-pass.json"
	    },	    
	    {
                name        =>  "warning",
                inputfiles  =>  "$Bin/inputs/hethom-warning,$Bin/inputs/parameters.txt",
                modfile     =>  "$Bin/../../../../../lib/Logic/Fork/HetHom.pm",
                expectedfile=>  "$Bin/inputs/hethom-warning.json"
	    },	    
	    {
                name        =>  "failure",
                inputfiles  =>  "$Bin/inputs/hethom-failure,$Bin/inputs/parameters.txt",
                modfile     =>  "$Bin/../../../../../lib/Logic/Fork/HetHom.pm",
                expectedfile=>  "$Bin/inputs/hethom-failure.json"
	    }	    
    ];
    foreach my $test (@$tests) {
		my $name            =   $test->{name};
		my $inputfiles      =   $test->{inputfiles};
		my $modfile         =   $test->{modfile};
		my $expectedfile        =   $test->{expectedfile};
		$self->logDebug("inputfiles", $inputfiles);
		$self->logDebug("modfile", $modfile);
		$self->logDebug("expectedfile", $expectedfile);
		$self->loadModfile($modfile);

		my $expected=	$self->fileJson($expectedfile);

		my ($index, $actual) = $self->selectIndex($inputfiles);
		$self->logDebug("name '$name' expected", $expected);
		$self->logDebug("actual", $actual);

		is_deeply($actual, $expected, $name);
	}
	$self->logDebug("completed");
}

#### UTILS
method getJsonParser {
	return $self->jsonparser() if defined $self->jsonparser();
	my $jsonparser = JSON->new();
	$self->jsonparser($jsonparser);

	return $self->jsonparser();
}

method fileJson ($inputfile) {
	my $contents = $self->fileContents($inputfile);

	my $parser = $self->getJsonParser();
	return $parser->decode($contents);
}



}   #### END MODULE           

print "Completed /a/t/unit/lib/Test/Logic.pm\n";

