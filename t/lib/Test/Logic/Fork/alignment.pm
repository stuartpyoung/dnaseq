use MooseX::Declare;

class Test::Logic::Fork::alignment with (Test::Agua::Common, Test::Agua::Common::Util, Agua::Common) extends Logic::Fork {

use Test::More;
use FindBin qw($Bin);

# Ints
has 'log'		=>  ( isa => 'Int', is => 'rw', default => 4 );
has 'printlog'	=>  ( isa => 'Int', is => 'rw' );

# STRINGS
has 'dumpfile'	=> ( isa => 'Str|Undef', is => 'rw', default => '' );

# OBJECTS
has 'db'		=> ( isa => 'Agua::DBase::MySQL', is => 'rw', required => 0 );
has 'conf' 	=> (
	is =>	'rw',
	'isa' => 'Conf::Yaml',
	default	=>	sub { Conf::Yaml->new( backup	=>	1 );	}
);

####////}}

method testPrintOutputfile {
	diag("printOutputfile");

	my $tests = [
		{
			name		=>	"pass",
			inputfiles	=>	"$Bin/inputs/alignment-pass.txt,$Bin/inputs/parameters.txt",
			modfile		=>	"$Bin/../../../../../lib/Logic/Fork/Alignment.pm",
			outputfile=>	"$Bin/outputs/alignment-pass.tsv",
			expectedfile=>	"$Bin/inputs/alignment-pass.tsv"
		}
		,
		{
			name		=>	"warning",
			inputfiles	=>	"$Bin/inputs/alignment-warning.txt,$Bin/inputs/parameters.txt",
			modfile		=>	"$Bin/../../../../../lib/Logic/Fork/Alignment.pm",
			outputfile=>	"$Bin/outputs/alignment-warning.tsv",
			expectedfile=>	"$Bin/inputs/alignment-warning.tsv"
		}
		,
		{
			name		=>	"failure",
			inputfiles	=>	"$Bin/inputs/alignment-failure.txt,$Bin/inputs/parameters.txt",
			modfile		=>	"$Bin/../../../../../lib/Logic/Fork/Alignment.pm",
			outputfile=>	"$Bin/outputs/alignment-failure.tsv",
			expectedfile=>	"$Bin/inputs/alignment-failure.tsv"
		}
	];
	
	foreach my $test ( @$tests ) {
		my $name		=	$test->{name};
		my $inputfiles	=	$test->{inputfiles};
		my $modfile		=	$test->{modfile};
		my $outputfile	=	$test->{outputfile};
		my $expectedfile=	$test->{expectedfile};
		$self->logDebug("expectedfile", $expectedfile);
		$self->logDebug("inputfiles", $inputfiles);
		$self->logDebug("modfile", $modfile);
		
		$self->loadModfile($modfile);	
		
		my ($index, $results) = $self->selectIndex($inputfiles);
		$self->logDebug("results", $results);
		
		$self->printOutputfile($outputfile, $results);
		
		ok($self->diff($expectedfile, $outputfile), $name);
	}
	$self->logDebug("completed");
}

method testSelectIndex {
	diag("selectIndex");

	$self->logDebug("");

	my $tests = [
		{
			name		=>	"pass",
			inputfiles	=>	"$Bin/inputs/alignment-pass.txt,$Bin/inputs/parameters.txt",
			modfile		=>	"$Bin/../../../../../lib/Logic/Fork/Alignment.pm",
			expectedfile=>	"$Bin/inputs/alignment-pass.json"
		}
		,
		{
			name		=>	"warning",
			inputfiles	=>	"$Bin/inputs/alignment-warning.txt,$Bin/inputs/parameters.txt",
			modfile		=>	"$Bin/../../../../../lib/Logic/Fork/Alignment.pm",
			expectedfile=>	"$Bin/inputs/alignment-warning.json"
		}
		,
		{
			name		=>	"failure",
			inputfiles	=>	"$Bin/inputs/alignment-failure.txt,$Bin/inputs/parameters.txt",
			modfile		=>	"$Bin/../../../../../lib/Logic/Fork/Alignment.pm",
			expectedfile=>	"$Bin/inputs/alignment-failure.json"
		}
	];
	
	foreach my $test ( @$tests ) {
		my $name		=	$test->{name};
		my $inputfiles	=	$test->{inputfiles};
		my $modfile		=	$test->{modfile};
		my $expectedfile=	$test->{expectedfile};
		$self->logDebug("inputfiles", $inputfiles);
		$self->logDebug("modfile", $modfile);
		
		$self->loadModfile($modfile);	
		
		my $expected=	$self->fileJson($expectedfile);

		my ($index, $actual) = $self->selectIndex($inputfiles);
		$self->logDebug("expectedfile", $expectedfile);
		$self->logDebug("expected", $expected);
		$self->logDebug("actual", $actual);
		
		is_deeply($actual, $expected, $name);
	}
	$self->logDebug("completed");
}


#### UTILS
method getJsonParser {
	return $self->jsonparser() if defined $self->jsonparser();
	my $jsonparser = JSON->new();
	$self->jsonparser($jsonparser);

	return $self->jsonparser();
}

method fileJson ($inputfile) {
	my $contents = $self->fileContents($inputfile);

	my $parser = $self->getJsonParser();
	return $parser->decode($contents);
}


}   #### END MODULE

print "Completed /a/t/unit/lib/Test/Logic.pm\n";
