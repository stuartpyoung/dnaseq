use MooseX::Declare;

class Test::Logic::Fork::relatedness with (Test::Agua::Common, Agua::Common) extends Logic::Fork {

use Test::More;
use FindBin qw($Bin);

# Ints
has 'log'		=>  ( isa => 'Int', is => 'rw', default => 4 );
has 'printlog'	=>  ( isa => 'Int', is => 'rw' );

# STRINGS
has 'dumpfile'	=> ( isa => 'Str|Undef', is => 'rw', default => '' );

# OBJECTS
has 'db'		=> ( isa => 'Agua::DBase::MySQL', is => 'rw', required => 0 );
has 'conf' 	=> (
	is =>	'rw',
	'isa' => 'Conf::Yaml',
	default	=>	sub { Conf::Yaml->new( backup	=>	1 );	}
);

####////}}

method testFamilyIsComplete {
	diag("familyIsComplete");
	my $tests = [
		{
			name			=>	"simple",
			modfile			=>	"$Bin/../../../../../lib/Logic/Fork/Relatedness.pm",
			uuid			=>	"LP6005599-DNA_A02",
			tsvfile			=> 	"$Bin/inputs/sample.tsv",
			expected 		=>	1
		}
	];
	
	foreach my $test ( @$tests ) {
		$self->logDebug("test", $test);
		my $name			=	$test->{name};
		my $uuid			=	$test->{uuid};
		my $tsvfile			=	$test->{tsvfile};
		my $modfile			=	$test->{modfile};
		my $expected		=	$test->{expected};
		$self->logDebug("name '$name' expected", $expected);
		$self->logDebug("expected", $expected);
		
		$self->loadModfile($modfile);	
		
		$self->loadTsvFile("sample", $tsvfile);
		
		my $actual = $self->familyIsComplete($uuid);
		$self->logDebug("actual", $actual);
		
		is_deeply($actual, $expected, $name);
	}
	$self->logDebug("completed");
	
}

method testGetMetrics {
	diag("getMetrics");
	
	my $tests = [
		{
			name	=>	"simple",
			modfile		=>	"$Bin/../../../../../lib/Logic/Fork/relatedness.pm",
			parameterfile => "$Bin/inputs/test-parameters.txt",
			expected =>		{
				"Per base sequence quality" => [
					[
						"failure", "lower quartile", "5"
					],
					[
						"failure", "lower quartile", "20"
					],
					[
						"warning", "lower quartile", "10"
					],
					[
						"warning", "lower quartile", "25"
					]
				],
				"Per sequence quality scores" => [
					[
						"warning", "observed mean quality", "27"
					]
				]
			}
		}
	];
	
	foreach my $test ( @$tests ) {
		$self->logDebug("test", $test);
		my $name			=	$test->{name};
		my $modfile			=	$test->{modfile};
		my $parameterfile	=	$test->{parameterfile};
		my $expected		=	$test->{expected};
		$self->logDebug("name '$name' expected", $expected);
		$self->logDebug("parameterfile", $parameterfile);
		$self->logDebug("expected", $expected);
		
		$self->loadModfile($modfile);	
		
		my $actual = $self->getMetrics($parameterfile);
		$self->logDebug("actual", $actual);
		
		is_deeply($actual, $expected, $name);
	}
	$self->logDebug("completed");
}

method testGetBlocks {
	diag("getBlocks");
	my $tests = [
		{
			name			=>	"simple",
			modfile			=>	"$Bin/../../../../../lib/Logic/Fork/FastQC.pm",
			datafile 		=> 	"$Bin/inputs/test-data.txt",
			metrics 		=>	{
				"Per base sequence quality" => [],
				"Per sequence quality scores" => [],
				"Per base sequence content" => []
			},
			expected 		=>	{
				"Per base sequence quality" => qq{#Base	Mean	Median	Lower Quartile	Upper Quartile	10th Percentile	90th Percentile
1	31.957806377505957	33.0	33.0	33.0	33.0	33.0
2	31.994190054209138	33.0	33.0	33.0	33.0	33.0
3	31.90502933582577	33.0	33.0	33.0	27.0	33.0},
			   "Per sequence quality scores" => qq{#Quality	Count
11	1.0
12	2.0
32	6261100.0
33	8075441.0
36	7.2410465E7},
				"Per base sequence content" =>  qq{#Base	G	A	T	C
1	23847827	46958590	44430993	28533865
2	25186490	45132818	47663866	25794968
3	23755654	45255539	44104361	30663682
115-119	26990347	45625295	43719509	27432397
120-124	27039837	43588258	43692857	29446036
125-126	29047422	43526581	43786593	27405750}
			}
		}
	];
	
	foreach my $test ( @$tests ) {
		$self->logDebug("test", $test);
		my $name			=	$test->{name};
		my $modfile			=	$test->{modfile};
		my $datafile		=	$test->{datafile};
		my $metrics			=	$test->{metrics};
		my $expected		=	$test->{expected};
		$self->logDebug("name '$name' expected", $expected);
		$self->logDebug("datafile", $datafile);
		$self->logDebug("expected", $expected);
		
		$self->loadModfile($modfile);	
		
		my $actual = $self->getBlocks($datafile, $metrics);
		$self->logDebug("actual", $actual);
		
		is_deeply($actual, $expected, $name);
	}
	$self->logDebug("completed");
}

method testSelectIndex {
	diag("selectIndex");

	$self->logDebug("");

	my $tests = [
		{
			name		=>	"pass",
			inputfiles	=>	"$Bin/inputs/relatedness1/relatedness_data.txt,$Bin/inputs/relatedness1/parameters.txt",
			modfile		=>	"$Bin/../../../../../lib/Logic/Fork/relatedness.pm",
			expected	=>	0
		},
		{
			name		=>	"warning",
			inputfiles	=>	"$Bin/inputs/relatedness1/relatedness_data-warning.txt,$Bin/inputs/relatedness1/parameters.txt",
			modfile		=>	"$Bin/../../../../../lib/Logic/Fork/relatedness.pm",
			expected	=>	1
		},
		{
			name		=>	"failure",
			inputfiles	=>	"$Bin/inputs/relatedness1/relatedness_data-failure.txt,$Bin/inputs/relatedness1/parameters.txt",
			modfile		=>	"$Bin/../../../../../lib/Logic/Fork/relatedness.pm",
			expected	=>	2
		}
	];
	
	foreach my $test ( @$tests ) {
		my $name		=	$test->{name};
		my $inputfiles	=	$test->{inputfiles};
		my $modfile		=	$test->{modfile};
		my $expected	=	$test->{expected};
		$self->logDebug("inputfiles", $inputfiles);
		$self->logDebug("modfile", $modfile);
		$self->logDebug("name '$name' expected", $expected);
		
		$self->loadModfile($modfile);	
		
		my $actual = $self->selectIndex($inputfiles);
		$self->logDebug("name '$name' expected", $expected);
		$self->logDebug("actual", $actual);
		
		ok($actual eq $expected, $name);
	}
	$self->logDebug("completed");
}

method testCheckPerBaseSequenceQuality {
    diag("checkPerBaseSequenceQuality");

	my $tests = [
		{
			name	=>	"simple",
			modfile		=>	"$Bin/../../../../../lib/Logic/Fork/relatedness.pm",
			rules => [
				["failure", "Lower Quartile", "5"],
				["failure", "Upper Quartile", "20"],
				["warning", "Lower Quartile", "10"],
				["warning", "Upper Quartile", "25"]
			],
			block => qq{#Base	Mean	Median	Lower Quartile	Upper Quartile	10th Percentile	90th Percentile
1	31.957806377505957	33.0	33.0	33.0	33.0	33.0
2	31.994190054209138	33.0	33.0	33.0	33.0	33.0
3	31.90502933582577	33.0	33.0	33.0	27.0	33.0
80-84	34.4101474280856	37.0	37.0	37.0	27.0	37.0
85-89	34.278324200016165	37.0	37.0	37.0	27.0	37.0
90-94	34.15208676834112	37.0	37.0	37.0	27.0	37.0
95-99	33.958661173728345	37.0	37.0	37.0	26.0	37.0
100-104	33.70497813120725	37.0	33.8	37.0	22.0	37.0
105-109	33.488958029551796	37.0	33.0	37.0	20.4	37.0
110-114	33.305040130094866	37.0	33.0	37.0	14.0	37.0
115-119	32.951729782666085	37.0	33.0	37.0	14.0	37.0
120-124	32.867116566053106	37.0	33.0	37.0	14.0	37.0
125-126	31.420966503832013	37.0	27.5	37.0	14.0	37.0},
			expected => 0
		}
	];
    
	foreach my $test ( @$tests ) {
		my $name 	= 	$test->{name};
		my $modfile	=	$test->{modfile};
		my $rules 	= 	$test->{rules};
		my $block 	= 	$test->{block};
		my $expected=	$test->{expected};
		$self->logDebug("block", $block);
		$self->logDebug("rules", $rules);
		
		$self->loadModfile($modfile);	
		
		my $actual = $self->checkPerBaseSequenceQuality($block, $rules);
		$self->logDebug("actual", $actual);
		$self->logDebug("expected", $expected);
		
		ok($actual == $expected, $name);
	}
}

method testCheckPerSequenceQualityScores {
    diag("checkPerSequenceQualityScores");

	my $tests = [
		{
			name	=>	"pass",
			modfile		=>	"$Bin/../../../../../lib/Logic/Fork/relatedness.pm",
			rules => [
				["warning", "observed mean quality", "27"],
				["failure", "observed mean quality", "20"]
			],
			block => qq{#Quality	Count
11	1.0
12	2.0
13	6.0
14	199.0
15	5641.0
16	9180.0
17	12501.0
34	1.1406332E7
35	2.0351679E7
36	7.2410465E7},
			expected => 0
		},
		{
			name	=>	"warning",
			modfile		=>	"$Bin/../../../../../lib/Logic/Fork/relatedness.pm",
			rules => [
				["warning", "observed mean quality", "36"],
				["failure", "observed mean quality", "20"]
			],
			block => qq{#Quality	Count
11	1.0
12	2.0
13	6.0
14	199.0
15	5641.0
16	9180.0
17	12501.0
34	1.1406332E7
35	2.0351679E7
36	7.2410465E7},
			expected => 1
		},
		{
			name	=>	"failure",
			modfile		=>	"$Bin/../../../../../lib/Logic/Fork/relatedness.pm",
			rules => [
				["warning", "observed mean quality", "40"],
				["failure", "observed mean quality", "36"]
			],
			block => qq{#Quality	Count
11	1.0
12	2.0
13	6.0
14	199.0
15	5641.0
16	9180.0
17	12501.0
34	1.1406332E7
35	2.0351679E7
36	7.2410465E7},
			expected => 2
		}
	];
    
	foreach my $test ( @$tests ) {
		my $name 	= $test->{name};
		my $modfile	=	$test->{modfile};
		my $rules 	= $test->{rules};
		my $block 	= $test->{block};
		my $expected=	$test->{expected};
		$self->logDebug("block", $block);
		$self->logDebug("rules", $rules);
		
		$self->loadModfile($modfile);	
		
		my $actual = $self->checkPerSequenceQualityScores($block, $rules);
		$self->logDebug("actual", $actual);
		$self->logDebug("expected", $expected);
		
		ok($actual == $expected, $name);
	}
}


method testCheckPerBaseSequenceContent {
    diag("checkPerBaseSequenceContent");

	my $tests = [
		{
			name	=>	"pass",
			modfile		=>	"$Bin/../../../../../lib/Logic/Fork/relatedness.pm",
			rules => [
				["warning", "difference greater than", 15],
				["failure", "difference greater than", 25]
			],
			block => qq{#Base	G	A	T	C
1	23847827	46958590	44430993	28533865
2	25186490	45132818	47663866	25794968
3	23755654	45255539	44104361	30663682
4	27334924	43529493	46014966	26900089
5	29571797	44563052	44134863	25509369
6	27229150	47057355	42460862	27031666
7-104	28981927	43603241	43843825	27340512
105-109	26971237	43577263	45922649	27297710
110-114	26945215	43471183	43854677	29497119
115-119	26990347	45625295	43719509	27432397
120-124	27039837	43588258	43692857	29446036
125-126	29047422	43526581	43786593	27405750},
			expected => 0
		}
		,
		{
			name	=>	"warning",
			modfile		=>	"$Bin/../../../../../lib/Logic/Fork/relatedness.pm",
			rules => [
				["warning", "difference greater than", 10],
				["failure", "difference greater than", 20]
			],
			block => qq{#Base	G	A	T	C
1	23847827	46958590	44430993	28533865
2	25186490	45132818	47663866	25794968
3	23755654	45255539	44104361	30663682
4	27334924	43529493	46014966	26900089
5	29571797	44563052	44134863	25509369
6	27229150	47057355	42460862	27031666
7-104	28981927	43603241	43843825	27340512
105-109	26971237	43577263	45922649	27297710
110-114	26945215	43471183	43854677	29497119
115-119	26990347	45625295	43719509	27432397
120-124	27039837	43588258	43692857	29446036
125-126	29047422	43526581	43786593	27405750},
			expected => 1
		}
		,
		{
			name	=>	"pass",
			modfile		=>	"$Bin/../../../../../lib/Logic/Fork/relatedness.pm",
			rules => [
				["warning", "difference greater than", 5],
				["failure", "difference greater than", 10]
			],
			block => qq{#Base	G	A	T	C
1	23847827	46958590	44430993	28533865
2	25186490	45132818	47663866	25794968
3	23755654	45255539	44104361	30663682
4	27334924	43529493	46014966	26900089
5	29571797	44563052	44134863	25509369
6	27229150	47057355	42460862	27031666
7-104	28981927	43603241	43843825	27340512
105-109	26971237	43577263	45922649	27297710
110-114	26945215	43471183	43854677	29497119
115-119	26990347	45625295	43719509	27432397
120-124	27039837	43588258	43692857	29446036
125-126	29047422	43526581	43786593	27405750},
			expected => 2
		}
	];
    
	foreach my $test ( @$tests ) {
		my $name 	= 	$test->{name};
		my $modfile	=	$test->{modfile};
		my $rules 	= 	$test->{rules};
		my $block 	= 	$test->{block};
		my $expected=	$test->{expected};
		$self->logDebug("block", $block);
		$self->logDebug("rules", $rules);
		
		$self->loadModfile($modfile);	
		
		my $actual = $self->checkPerBaseSequenceContent($block, $rules);
		$self->logDebug("actual", $actual);
		$self->logDebug("expected", $expected);
		
		ok($actual == $expected, $name);
	}
}





method testCheckPerBaseGCContent {
	diag("checkPerBaseGCContent");

	my $tests = [
		{
			name	=>	"pass",
			modfile		=>	"$Bin/../../../../../lib/Logic/Fork/relatedness.pm",
			rules => [
				["warning", "deviation from normal", 15],
				["failure", "deviation from normal", 30]
			],
			block => qq{#Base	%GC
1	36.43404567428368
2	35.458420376582694
3	37.849231581672896
4	37.72097104376625
5	38.30958274103866
6	37.73903250552534
7-99	37.33092392366666
100-104	38.36698374461632
105-109	38.32954335298495
110-114	38.335894356688335
115-119	38.3237022764446
120-124	38.480121800280905
125-126	44.43026072753486
},
			expected => 0
		}
	];
    
	foreach my $test ( @$tests ) {
		my $name 	= 	$test->{name};
		my $modfile	=	$test->{modfile};
		my $rules 	= 	$test->{rules};
		my $block 	= 	$test->{block};
		my $expected=	$test->{expected};
		$self->logDebug("block", $block);
		$self->logDebug("rules", $rules);
		
		$self->loadModfile($modfile);	
		
		my $actual = $self->checkPerBaseGCContent($block, $rules);
		$self->logDebug("actual", $actual);
		$self->logDebug("expected", $expected);
		
		ok($actual == $expected, $name);
	}
}

method testGetDeviation {
	diag("getDeviation");

	my $tests = [
		{
			name	=>	"pass",
			modfile		=>	"$Bin/../../../../../lib/Logic/Fork/relatedness.pm",
			rules => [
				["warning", "deviation from normal", 15],
				["failure", "deviation from normal", 30]
			],
			block => qq{1	4
2	2
3	5
4	8
5	6},
			expected => 0
		}
	];
    
	foreach my $test ( @$tests ) {
		my $name 	= 	$test->{name};
		my $modfile	=	$test->{modfile};
		my $rules 	= 	$test->{rules};
		my $block 	= 	$test->{block};
		my $expected=	$test->{expected};
		$self->logDebug("block", $block);
		$self->logDebug("rules", $rules);
		
		$self->loadModfile($modfile);	
	
		my $lines 		= 	[ split "\n", $block ];
		my $columnindexes= 	{ "%GC" => 1 };
		my $counts		= 	{ "%GC" => 0 };
		my $total 		=	0;
		($counts, $total) = $self->getColumnCounts($lines, $columnindexes, $counts);
		$self->logDebug("counts", $counts);
	    my $average = $counts->{"%GC"} / $total;
		$self->logDebug("average", $average);
		
		my $actual = $self->getDeviation($block, $average, $columnindexes);
		$self->logDebug("actual", $actual);
		$self->logDebug("expected", $expected);
		
		ok($actual == $expected, $name);
	}
}

method testCheckPerBaseNContent {
	diag("checkPerBaseNContent");

	my $tests = [
		{
			name	=>	"pass",
			modfile		=>	"$Bin/../../../../../lib/Logic/Fork/relatedness.pm",
			rules => [
				["warning", "N-Count", 30],
				["failure", "N-Count", 40]
			],
			block => qq{#Base	N-Count
1	0.006067603324993763
2	0.0012915565537039682
3	5.30671863476644E-4
4	3.665322045244972E-4
5	6.384754530426725E-4
6	6.718597904566685E-4
7-126	23.649395768878815},
			expected => 0
		},
		{
			name	=>	"warning",
			modfile		=>	"$Bin/../../../../../lib/Logic/Fork/relatedness.pm",
			rules => [
				["warning", "N-Count", 20],
				["failure", "N-Count", 30]
			],
			block => qq{#Base	N-Count
1	0.006067603324993763
2	0.0012915565537039682
3	5.30671863476644E-4
4	3.665322045244972E-4
5	6.384754530426725E-4
6	6.718597904566685E-4
7-126	23.649395768878815},
			expected => 1
		},
		{
			name	=>	"failure",
			modfile		=>	"$Bin/../../../../../lib/Logic/Fork/relatedness.pm",
			rules => [
				["warning", "N-Count", 10],
				["failure", "N-Count", 20]
			],
			block => qq{#Base	N-Count
1	0.006067603324993763
2	0.0012915565537039682
3	5.30671863476644E-4
4	3.665322045244972E-4
5	6.384754530426725E-4
6	6.718597904566685E-4
7-126	23.649395768878815},
			expected => 2
		}
	];
    
	foreach my $test ( @$tests ) {
		my $name 	= 	$test->{name};
		my $modfile	=	$test->{modfile};
		my $rules 	= 	$test->{rules};
		my $block 	= 	$test->{block};
		my $expected=	$test->{expected};
		$self->logDebug("block", $block);
		$self->logDebug("rules", $rules);
		
		$self->loadModfile($modfile);	
		
		my $actual = $self->checkPerBaseNContent($block, $rules);
		$self->logDebug("actual", $actual);
		$self->logDebug("expected", $expected);
		
		ok($actual == $expected, $name);
	}
}

method testCheckSequenceDuplicationLevels {
	diag("checkSequenceDuplicationLevels");

	my $tests = [
		{
			name	=>	"pass",
			modfile		=>	"$Bin/../../../../../lib/Logic/Fork/relatedness.pm",
			rules => [
				["warning", "Relative count", 20],
				["failure", "Relative count", 50]
			],
			block => qq{#Total Duplicate Percentage	9.541481566235685
#Duplication Level	Relative count
1	100.0
2	3.749511925515709
3	0.5449935931631076
4	0.31236766994616055
5	0.23262592321694706
6	0.17763161512783426
7	0.15123434724506013
8	0.12978656709030614
9	0.11328827466357232
10++	4.577176262256857
},
			expected => 0
		},
		{
			name	=>	"warning",
			modfile		=>	"$Bin/../../../../../lib/Logic/Fork/relatedness.pm",
			rules => [
				["warning", "Relative count", 20],
				["failure", "Relative count", 50]
			],
			block => qq{#Total Duplicate Percentage	29.541481566235685
#Duplication Level	Relative count
1	100.0
2	3.749511925515709
3	0.5449935931631076
4	0.31236766994616055
5	0.23262592321694706
6	0.17763161512783426
7	0.15123434724506013
8	0.12978656709030614
9	0.11328827466357232
10++	4.577176262256857
},
			expected => 1
		},
		{
			name	=>	"failure",
			modfile		=>	"$Bin/../../../../../lib/Logic/Fork/relatedness.pm",
			rules => [
				["warning", "Relative count", 20],
				["failure", "Relative count", 50]
			],
			block => qq{#Total Duplicate Percentage	59.541481566235685
#Duplication Level	Relative count
1	100.0
2	3.749511925515709
3	0.5449935931631076
4	0.31236766994616055
5	0.23262592321694706
6	0.17763161512783426
7	0.15123434724506013
8	0.12978656709030614
9	0.11328827466357232
10++	4.577176262256857
},
			expected => 2
		}
	];
    
	foreach my $test ( @$tests ) {
		my $name 	= 	$test->{name};
		my $modfile	=	$test->{modfile};
		my $rules 	= 	$test->{rules};
		my $block 	= 	$test->{block};
		my $expected=	$test->{expected};
		$self->logDebug("block", $block);
		$self->logDebug("rules", $rules);
		
		$self->loadModfile($modfile);	
		
		my $actual = $self->checkSequenceDuplicationLevels($block, $rules);
		$self->logDebug("actual", $actual);
		$self->logDebug("expected", $expected);
		
		ok($actual == $expected, $name);
	}
}


method testCheckOverrepresentedSequences {
    diag("checkOverrepresentedSequences");

	my $tests = [
		{
			name	=>	"pass",
			modfile		=>	"$Bin/../../../../../lib/Logic/Fork/relatedness.pm",
			rules => [
				["warning", "Percentage", 5],
				["failure", "Percentage", 10]
			],
			block => qq{#Sequence	Count	Percentage	Possible Source
ATCGGAAGAGCACACGTCTGAACTCCAGTCACGAGATTCCATCTCGTATG	1580304	1.0991125406809885	TruSeq Adapter, Index 7 (97% over 37bp)
ATCGGAAGAGCACACGTCTGAACTCCAGTCACGAGATTCCAACTCGTATG	151887	0.10563847618332506	TruSeq Adapter, Index 7 (97% over 37bp)},
			expected => 0
		}
		,
		{
			name	=>	"warning",
			modfile		=>	"$Bin/../../../../../lib/Logic/Fork/relatedness.pm",
			rules => [
				["warning", "Percentage", 0.1],
				["failure", "Percentage", 5]
			],
			block => qq{#Sequence	Count	Percentage	Possible Source
ATCGGAAGAGCACACGTCTGAACTCCAGTCACGAGATTCCATCTCGTATG	1580304	1.0991125406809885	TruSeq Adapter, Index 7 (97% over 37bp)
ATCGGAAGAGCACACGTCTGAACTCCAGTCACGAGATTCCAACTCGTATG	151887	0.10563847618332506	TruSeq Adapter, Index 7 (97% over 37bp)},
			expected => 1
		},
		{
			name	=>	"failure",
			modfile		=>	"$Bin/../../../../../lib/Logic/Fork/relatedness.pm",
			rules => [
				["warning", "Percentage", 0.1],
				["failure", "Percentage", 1]
			],
			block => qq{#Sequence	Count	Percentage	Possible Source
ATCGGAAGAGCACACGTCTGAACTCCAGTCACGAGATTCCATCTCGTATG	1580304	1.0991125406809885	TruSeq Adapter, Index 7 (97% over 37bp)
ATCGGAAGAGCACACGTCTGAACTCCAGTCACGAGATTCCAACTCGTATG	151887	0.10563847618332506	TruSeq Adapter, Index 7 (97% over 37bp)},
			expected => 2
		}
	];
    
	foreach my $test ( @$tests ) {
		my $name 	= 	$test->{name};
		my $modfile	=	$test->{modfile};
		my $rules 	= 	$test->{rules};
		my $block 	= 	$test->{block};
		my $expected=	$test->{expected};
		$self->logDebug("block", $block);
		$self->logDebug("rules", $rules);
		
		$self->loadModfile($modfile);	
		
		my $actual = $self->checkOverrepresentedSequences($block, $rules);
		$self->logDebug("actual", $actual);
		$self->logDebug("expected", $expected);
		
		ok($actual == $expected, $name);
	}	
}



}   #### END MODULE

print "Completed /a/t/unit/lib/Test/Logic.pm\n";
