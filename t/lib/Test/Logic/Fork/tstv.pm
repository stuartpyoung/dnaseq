use MooseX::Declare;

class Test::Logic::Fork::tstv with (Test::Agua::Common, Agua::Common) extends Logic::Fork {

use Test::More;
use FindBin qw($Bin);

# Ints
has 'log'=>  ( isa => 'Int', is => 'rw', default => 4 );
has 'printlog'=>  ( isa => 'Int', is => 'rw' );

# STRINGS
has 'dumpfile'=> ( isa => 'Str|Undef', is => 'rw', default => '' );

# OBJECTS
has 'db'=> ( isa => 'Agua::DBase::MySQL', is => 'rw', required => 0 );
has 'conf' => (
is =>'rw',
'isa' => 'Conf::Yaml',
default=>sub { Conf::Yaml->new( backup=>1 );});

method testSelectIndex {
	diag("selectIndex");

	$self->logDebug("");

	my $tests = [
	    {
                name        =>  "pass",
                inputfiles  =>  "$Bin/inputs/test.tstv-pass,$Bin/inputs/parameters.txt",
                modfile     =>  "$Bin/../../../../../lib/Logic/Fork/TsTv.pm",
                expectedfile=>  "$Bin/inputs/test.tstv-pass.json"
	    },
	    {
                name        =>  "warning",
                inputfiles  =>  "$Bin/inputs/test.tstv-warning,$Bin/inputs/parameters.txt",
                modfile     =>  "$Bin/../../../../../lib/Logic/Fork/TsTv.pm",
                expectedfile=>  "$Bin/inputs/test.tstv-warning.json"
	    },	    
	    {
                name        =>  "failure",
                inputfiles  =>  "$Bin/inputs/test.tstv-failure,$Bin/inputs/parameters.txt",
                modfile     =>  "$Bin/../../../../../lib/Logic/Fork/TsTv.pm",
                expectedfile=>  "$Bin/inputs/test.tstv-failure.json"
	    }	    
    ];

    foreach my $test (@$tests) {
		my $name            =   $test->{name};
		my $inputfiles      =   $test->{inputfiles};
		my $modfile         =   $test->{modfile};
		my $expectedfile    =   $test->{expectedfile};
		$self->logDebug("inputfiles", $inputfiles);
		$self->logDebug("modfile", $modfile);
		$self->logDebug("name '$name' expectedfile", $expectedfile);
		$self->loadModfile($modfile);

		my $expected=	$self->fileJson($expectedfile);

		my ($index, $actual) = $self->selectIndex($inputfiles);
		$self->logDebug("expected", $expected);
		$self->logDebug("actual", $actual);

		is_deeply($actual, $expected, $name);
	}
	$self->logDebug("completed");
}

#### UTILS
method getJsonParser {
	return $self->jsonparser() if defined $self->jsonparser();
	my $jsonparser = JSON->new();
	$self->jsonparser($jsonparser);

	return $self->jsonparser();
}

method fileJson ($inputfile) {
	my $contents = $self->fileContents($inputfile);

	my $parser = $self->getJsonParser();
	return $parser->decode($contents);
}


}   #### END MODULE           

print "Completed /a/t/unit/lib/Test/Logic.pm\n";

