use Moose::Util::TypeConstraints;
use MooseX::Declare;
use Method::Signatures::Modifiers;

class Test::SampleSheet extends SampleSheet with (Logger, Test::Agua::Common::Util) {

use FindBin qw($Bin);
use Test::More;

#####/////}}}}}

method BUILD ($args) {
	if ( defined $args ) {
		foreach my $arg ( $args ) {
			$self->$arg($args->{$arg}) if $self->can($arg);
		}
	}
	$self->logDebug("args", $args);
}

method testConvert {
	diag("convert");
	
	my $tests = [
		{
			name			=>	"simple conversion",
			args			=> 	{
				inputfile		=>	"$Bin/inputs/hpv/samplesheetorig.csv",
				outputfile		=>	"$Bin/outputs/hpv/samplesheet.csv",
				fcid			=> 	"C6FG8ANXX"		,
				sampleref		=> 	"hg19"			,
				control			=> 	"N"				,
				recipe			=> 	"PE_indexing"	,
				operator		=> 	"AP"			,
				sampleproject	=>	"HPVtest" 	
			},
			expectedfile	=>	"$Bin/inputs/SampleSheet.csv"
		}
		#,
		#{
		#	name			=>	"simple conversion",
		#	args			=> 	{
		#		inputfile		=>	"$Bin/inputs/SampleSheetOrig.csv",
		#		outputfile		=>	"$Bin/outputs/SampleSheet.csv",
		#		fcid			=> 	"C6FG8ANXX"		,
		#		sampleref		=> 	"hg19"			,
		#		control			=> 	"N"				,
		#		recipe			=> 	"PE_indexing"	,
		#		operator		=> 	"AP"			,
		#		sampleproject	=>	"WgsTestRun1" 	
		#	},
		#	expectedfile	=>	"$Bin/inputs/SampleSheet.csv"
		#}
		#,
		#{
		#	name			=>	"replace inplace",
		#	args			=> 	{
		#		inputfile		=>	"$Bin/inputs/SampleSheetOrig2.csv",
		#		fcid			=> 	"C6FG8ANXX"		,
		#		sampleref		=> 	"hg19"			,
		#		control			=> 	"N"				,
		#		recipe			=> 	"PE_indexing"	,
		#		operator		=> 	"AP"			,
		#		sampleproject	=>	"WgsTestRun1" 	
		#	},
		#	expectedfile	=>	"$Bin/inputs/SampleSheet2.csv"
		#}
	];
	
	foreach my $test ( @$tests ) {
		my $name 		= 	$test->{name};
		my $args 		= 	$test->{args};
		my $expectedfile=	$test->{expectedfile};
		my $outputfile	=	$args->{outputfile};
		
		#### COPY TO outputs IF NO outputfile
		if ( not defined $outputfile ) {
			my $inputfile = $args->{inputfile};
			$outputfile = $inputfile;
			$outputfile =~ s/inputs/outputs/;
			`cp $inputfile $outputfile`;
			$args->{inputfile} = $outputfile;			
		}
		
		$self->logDebug("name", $name);
		$self->logDebug("args", $args);
		$self->convert($args);
		
		my $diff = $self->diff($outputfile, $expectedfile);
		$self->logDebug("diff", $diff);
		
		ok($diff, $name);
	}
}



}	