#!/usr/bin/perl -w

=head2
	
APPLICATION 	test.t

PURPOSE

	Test Relatedness module
	
NOTES

	1. RUN AS ROOT
	
	2. BEFORE RUNNING, SET ENVIRONMENT VARIABLES, E.G.:
	
		export installdir=/aguadev

=cut

use Test::More 	tests => 28;
use Getopt::Long;
use FindBin qw($Bin);
use lib "$Bin/../../lib";
use lib "$Bin/../../../lib";

BEGIN
{
    my $installdir = $ENV{'installdir'} || "/a";
    unshift(@INC, "$installdir/extlib/lib/perl5");
    unshift(@INC, "$installdir/extlib/lib/perl5/x86_64-linux-gnu-thread-multi/");
    unshift(@INC, "$installdir/lib");
    unshift(@INC, "$installdir/t/common/lib");
    unshift(@INC, "$installdir/t/unit/lib");
    unshift(@INC, "$installdir/apps/dnaseq/lib");
    unshift(@INC, "$installdir/apps/dnaseq/t/lib");
}

##### CREATE OUTPUTS DIR
#my $outputsdir = "$Bin/outputs";
#`mkdir -p $outputsdir` if not -d $outputsdir;

BEGIN {
    use_ok('Conf::Yaml');
    use_ok('Relatedness');
    use_ok('Test::Relatedness');
}
require_ok('Conf::Yaml');
require_ok('Relatedness');
require_ok('Test::Relatedness');

#### SET CONF FILE
my $installdir  =   $ENV{'installdir'} || "/a";
my $configfile	=   "$installdir/conf/config.yml";

#### GET OPTIONS
my $logfile 	= "/tmp/test.fork.$$.log";
my $log     =   2;
my $printlog    =   5;
my $help;
GetOptions (
    'log=i'     => \$log,
    'printlog=i'    => \$printlog,
    'logfile=s'     => \$logfile,
    'help'          => \$help
) or die "No options specified. Try '--help'\n";
usage() if defined $help;

my $conf = Conf::Yaml->new(
    inputfile	=>	$configfile,
    memory      =>  1,
    backup	    =>	1,
    separator	=>	"\t",
    spacer	    =>	"\\s\+",
    logfile     =>  $logfile,
	log     =>  2,
	printlog    =>  5    
);
isa_ok($conf, "Conf::Yaml", "conf");

#### SET DUMPFILE
my $dumpfile    =   "$installdir/t/unit/dump/create.dump";

my $object = new Test::Relatedness(
    conf        =>  $conf,
    logfile     =>  $logfile,
    dumpfile    =>  $dumpfile,
	log			=>	$log,
	printlog    =>  $printlog
);
isa_ok($object, "Test::Relatedness", "object");

#### TESTS
$object->testFamilyIsComplete();

#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#                                    SUBROUTINES
#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

sub usage {
    print `perldoc $0`;
}

