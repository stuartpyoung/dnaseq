PASS	Basic Statistics	FX100ng_16pM_CTTGTA_L001_R1_001.fastq.gz
PASS	Per base sequence quality	FX100ng_16pM_CTTGTA_L001_R1_001.fastq.gz
PASS	Per sequence quality scores	FX100ng_16pM_CTTGTA_L001_R1_001.fastq.gz
PASS	Per base sequence content	FX100ng_16pM_CTTGTA_L001_R1_001.fastq.gz
WARN	Per base GC content	FX100ng_16pM_CTTGTA_L001_R1_001.fastq.gz
WARN	Per sequence GC content	FX100ng_16pM_CTTGTA_L001_R1_001.fastq.gz
FAIL	Per base N content	FX100ng_16pM_CTTGTA_L001_R1_001.fastq.gz
PASS	Sequence Length Distribution	FX100ng_16pM_CTTGTA_L001_R1_001.fastq.gz
PASS	Sequence Duplication Levels	FX100ng_16pM_CTTGTA_L001_R1_001.fastq.gz
WARN	Overrepresented sequences	FX100ng_16pM_CTTGTA_L001_R1_001.fastq.gz
WARN	Kmer Content	FX100ng_16pM_CTTGTA_L001_R1_001.fastq.gz
