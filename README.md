README

### This is the README file for dnaseq, the Broad GATK Best Practices DNA-Seq analysis pipeline. For installation instructions, see the INSTALL file.

## Introduction

This package consists of workflows for running the Broad GATK Best Practices (Broad BP) pipeline for DNA-Seq data and associated QC, pre-processing, alignment and variant calling steps. The workflow configuration is modular; you can run any one of the workflows separately, or chain together as many  as required.

### Workflows

1. QC

Run FastQC, alignment summary statistics, het/hom and ts/tv ratio analyses and filter for pass/fail according to criteria in the user-defined parameters.tsv file, e.g., dnaseq/data/tsv/fastqc/parameters.tsv.

2. Align

Run BWA on BAM file to align reads to reference genome (default: ucsc.hg19.fasta).

3. Broad Best Practices DNA-Seq

The Broad BP DNA-Seq workflow consists of the following steps:  

#### 1) Fix mates
#### 2) Filter reads to remove low quality reads
#### 3) Mark duplicates to remove multi-cloned reads
#### 4) Add read groups
#### 5) Filter reads based on quality
#### 6) Recalibrate indels
#### 7) Recalibrate bases
#### 8) Run Variant Caller

4. Variant Calling

Call variants from input BAM file using any combination of HaplotypeCaller, Freebayes and Varscan.


### Adding samples

To run the workflow on your sample data, you need to provide at least one unique ID per sample and enough additional information to locate the sample file in the filesystem. The file format is one line per sample with tab-separated values (TSV), e.g.:

dnaseq/data/sql/clinical97.tsv:

	1809aaea-094a-4d25-8218-e8a1013ddac3	1508UNHX-0016	GM12877	1	/data/bam	dnaseq.sample.data	dnaseq.sample.dataqc
	debc1401-4fc5-4732-93fc-eee1d3b37c51	1508UNHX-0016	GM12877	2	/data/bam	dnaseq.sample.data	idnaseq.sample.dataqc
	...

The columns of the sample file are defined in its accompanying SQL file, e.g.:

dnaseq/data/sql/clinical97.sql:

	CREATE TABLE IF NOT EXISTS clinical97
	(
	    username            VARCHAR(40) NOT NULL,
	    project             VARCHAR(40) NOT NULL,
	    sample              VARCHAR(255) NOT NULL,
	    run                 VARCHAR(255) NOT NULL,
	    subject             VARCHAR(255) NOT NULL,
	    ordinal             INT(4) NOT NULL,
	    outputdir           VARCHAR(255) NOT NULL,
	    sourcebucket        VARCHAR(255) NOT NULL,
	    targetbucket        VARCHAR(255) NOT NULL,
	    status              VARCHAR(40) NOT NULL,
	 
	    PRIMARY KEY  (username, project, sample)
	)


When an individual sample is run in a workflow, all of the fields in its sample table entry are available as inputs to the workflow's stages. They are accessed by inserting the fields in uppercase surrounded by angular brackets into the workflow configuration file. In this example, the 'run' and 'sample' field values for the particular samplewill be automatically inserted into the 'value' line for the 'source' application parameter: 

dnaseq/conf/work/qc/All.work

	...
	      -
	        ordinal: 3
	        description: 'Source file or directory'
	        discretion: required
	        paramname: source
	        value: s3://dnaseq.sample.data/<RUN>/<SAMPLE>/<SAMPLE>_SNP_INDEL.vcf

For your own custom analysis, you can create new *.tsv and *.sql files and add or remove columns in the *.tsv file while making sure to mirror these changes by adding or removing the corresponding fields in the *.sql file.
(You should verify the syntax of your *.sql file by pasting its contents into the MySQL command prompt.)

To load your sample data, run the following command:

	/a/bin/sample/loadSamples.pl \
	--username admin \
	--project QC \
	--table clinical97 \
	--sqlfile $INSTALLDIR/data/sql/clinical97.sql \
	--tsvfile $INSTALLDIR/data/tsv/clinical97.tsv \
	--log 4

... or a similar command with the names of your new *.tsv and *.sql files (e.g., clinicalsamples1.tsv and clinicalsamples1.sql, using table name 'clinicalsamples1').


### Running workflows

1. RAISE SOFT LIMIT FOR OPEN FILES TO 4096 (FOR samtools sort)  
  
ulimit -S -n 4096  
  
  
2. RUN ANALYSIS  
  
The following commands will run the specified project, workflow or stage for each of the the samples loaded into the database using the loadSamples.pl script (see above).


#### Project-level

You can run the whole Broad BP DNA-Seq pipeline project with a single command:  
  
flow runProject --project DnaSeq --username admin  --log 4 --start 1  


#### Workflow-level  
To run the workflows consecutively, execute the following commands:  
  
##### QC  

flow runWorkflow --project QC --workflow All --username admin --log 4   

##### Align  

flow runWorkflow --project Align --workflow Bwa --username admin --log 4   

##### DnaSeq  
  
1. FixMates  
flow runWorkflow --project DnaSeq --workflow FixMates --username admin --log 4   
  
2. FilterReads  
flow runWorkflow --project DnaSeq --workflow FilterReads --username admin --log 4   
  
3. MarkDuplicates  
flow runWorkflow --project DnaSeq --workflow MarkDuplicates --username admin --log 4 --samplestring  	  
  
4. AddReadGroups  
flow runWorkflow --project DnaSeq --workflow AddReadGroups --username admin --log 4   
	  
5. QualityFilter  
flow runWorkflow --project DnaSeq --workflow QualityFilter --username admin --log 4   
  
6. IndelRealignment  
flow runWorkflow --project DnaSeq --workflow IndelRealignment --username admin --log 4   
  
7. BaseRecalibration  
flow runWorkflow --project DnaSeq --workflow BaseRecalibration --username admin --log 4   
  
##### Variant

flow runWorkflow --project Variant --workflow HaplotypeCaller --username admin --log 4   
 
#### Stage-level
  
In addition to running the workflows consecutively, you can also run a single stage of a workflow. For example:  
  
1. Run FixMate stage of workflow FixMates  
flow runStage --project DnaSeq --workflow FixMates --username admin --log 4 --stagenumber 2    
  
2. Run realignTarget stage of workflow IndelRealignment   
flow runStage --project DnaSeq --workflow IndelRealignment --username admin --log 4 --stagenumber 1    
  
3. Run realign stage of workflow IndelRealignment   
flow runStage --project DnaSeq --workflow IndelRealignment --username admin --log 4 --stagenumber 2    


## Customizing workflows


Executables:

app 
work
flow  



