#!/bin/bash

STUDY=1506UNHX-0001


IPS=(54.159.108.198 54.144.49.95 54.162.251.214 54.81.196.220 54.196.118.12)

INDEXES=(0 1 2 3 4)

SAMPLES=(29246fb6-f34c-4112-a89f-c13837079291 4edec4dd-8873-4cc7-abd7-88450bbf8111 8e8f3e3c-c512-41b4-8ce4-69da096f3d8c aef225e7-2eb2-4c78-abe3-7023008ca37a 722a1e49-07fa-4c75-8285-3b5cca54e193)

SUFFIXES=(CollectAlignmentSummaryMetrics hethom TsTv.summary fastqc.zip)

for INDEX in ${INDEXES[@]};
do
    SAMPLE=${SAMPLES[$INDEX]}
    IP=${IPS[$INDEX]}
    echo $SAMPLE $IP
    for SUFFIX in ${SUFFIXES[@]};
    do
        #echo $SUFFIX
        mkdir -p /data/bam/$STUDY/$SAMPLE
        command="scp -r -i /home/syoung/.ssh/inova/bioinfo1/id_rsa ubuntu@$IP:/data/bam/$SAMPLE/*$SUFFIX /data/bam/$STUDY/$SAMPLE"
        echo $command
        #`$command`
        
        if [ $SUFFIX == "fastqc.zip" ]; then
            unzip /data/bam/$STUDY/$SAMPLE/*$SUFFIX -d /data/bam/$STUDY/$SAMPLE
        fi
    done;

done;

