#!/bin/bash

PreCatInput="GS_12_100ng_TGACCA_"
#INPUT=$PreCatInput"_L1to8"
REFERENCE=/data/reference/hpv/ucsc.hg19.hpv16.indexed.fasta
#$PATH=./
#export PATH=$PATH:./

#zcat \
#$PreCatInput"L001_R1_001.fastq.gz" \
#$PreCatInput"L002_R1_001.fastq.gz" \
#$PreCatInput"L003_R1_001.fastq.gz" \
#$PreCatInput"L004_R1_001.fastq.gz" \
#$PreCatInput"L005_R1_001.fastq.gz" \
#$PreCatInput"L006_R1_001.fastq.gz" \
#$PreCatInput"L007_R1_001.fastq.gz" \
#$PreCatInput"L008_R1_001.fastq.gz" \
#| gzip -c > $PreCatInput"L1to8_R1_001.fastq.gz"
#
#zcat \
#$PreCatInput"L001_R2_001.fastq.gz" \
#$PreCatInput"L002_R2_001.fastq.gz" \
#$PreCatInput"L003_R2_001.fastq.gz" \
#$PreCatInput"L004_R2_001.fastq.gz" \
#$PreCatInput"L005_R2_001.fastq.gz" \
#$PreCatInput"L006_R2_001.fastq.gz" \
#$PreCatInput"L007_R2_001.fastq.gz" \
#$PreCatInput"L008_R2_001.fastq.gz" \
#| gzip -c > $PreCatInput"L1to8_R2_001.fastq.gz"


#/usr/bin/time -o ./fastqc.usage -f "%Uuser %Ssystem %Eelapsed %PCPU (%Xtext+%Ddata %Mmax)k" \
#/a/apps/fastqc/0.9.3/fastqc --thread 16 $PreCatInput"L1to8_R1_001.fastq.gz" $PreCatInput"L1to8_R2_001.fastq.gz"
#

#/usr/bin/time -o Bwa.usage -f "%Uuser %Ssystem %Eelapsed %PCPU (%Xtext+%Ddata %Mmax)k" /a/apps/bwa/0.7.12/bwa mem -t 16 $REFERENCE \
#\
#$PreCatInput"L1to8_R1_001.fastq.gz" \
#\
#$PreCatInput"L1to8_R2_001.fastq.gz" \
#> \
#$PreCatInput"L1to8_R1a2_001.aln-pe.sam"
#
#
#/usr/bin/time -o ./SamToBam.usage -f "%Uuser %Ssystem %Eelapsed %PCPU (%Xtext+%Ddata %Mmax)k" \
#/a/apps/samtools/0.1.19/samtools view -bT $REFERENCE $PreCatInput"L1to8_R1a2_001.aln-pe.sam" >  $PreCatInput"L1to8_R1a2_001.aln-pe.bam"
#
#/usr/bin/time -o ./SortBam.usage -f "%Uuser %Ssystem %Eelapsed %PCPU (%Xtext+%Ddata %Mmax)k" \
#/a/apps/samtools/0.1.19/samtools sort $PreCatInput"L1to8_R1a2_001.aln-pe.bam" $PreCatInput"L1to8_R1a2_001.aln-pe.sorted"
#
#/usr/bin/time -o ./MarkDuplicates.usage -f "%Uuser %Ssystem %Eelapsed %PCPU (%Xtext+%Ddata %Mmax)k" \
#java -Xmx30g -Xms30g -jar /a/apps/picard/latest/dist/picard.jar MarkDuplicates \
#VALIDATION_STRINGENCY=LENIENT \
#INPUT=$PreCatInput"L1to8_R1a2_001.aln-pe.sorted"".bam" \
#OUTPUT= $PreCatInput"L1to8_R1a2_001.aln-pe.sorted"".markdup.bam" \
#METRICS_FILE= $PreCatInput"L1to8_R1a2_001_picard_markduplicates_metrics.txt" \
#REMOVE_DUPLICATES=false \
#ASSUME_SORTED=true
#
#/usr/bin/time -o ./IndexBam.usage -f "%Uuser %Ssystem %Eelapsed %PCPU (%Xtext+%Ddata %Mmax)k" \
#/a/apps/samtools/0.1.19/samtools index $PreCatInput"L1to8_R1a2_001.aln-pe.sorted.bam"
#
#
#/usr/bin/time -o ./Flagstat.usage -f "%Uuser %Ssystem %Eelapsed %PCPU (%Xtext+%Ddata %Mmax)k" \
#/a/apps/samtools/0.1.19/samtools flagstat $PreCatInput"L1to8_R1a2_001.aln-pe.sorted.bam" > $PreCatInput"L1to8_R1a2_001.aln-pe.sorted.bam"".flagstat.txt"
#
#/usr/bin/time -o ./CollectWgsMetrics.usage -f "%Uuser %Ssystem %Eelapsed %PCPU (%Xtext+%Ddata %Mmax)k" \
#java -Xmx30g -jar /a/apps/picard/latest/dist/picard.jar CollectWgsMetrics \
#VALIDATION_STRINGENCY=LENIENT \
#INPUT=$PreCatInput"L1to8_R1a2_001.aln-pe.sorted.bam" \
#OUTPUT=$PreCatInput"L1to8_R1a2_001.aln-pe.sorted.bam""_CollectWgsMetrics.txt" \
#REFERENCE_SEQUENCE=$REFERENCE
#
#/usr/bin/time -o ./CollectAlignmentSummaryMetrics.usage -f "%Uuser %Ssystem %Eelapsed %PCPU (%Xtext+%Ddata %Mmax)k" \
#java -Xmx30g -Xms30g -jar /a/apps/picard/latest/dist/picard.jar CollectAlignmentSummaryMetrics \
#METRIC_ACCUMULATION_LEVEL=ALL_READS \
#INPUT=$PreCatInput"L1to8_R1a2_001.aln-pe.sorted.bam" \
#OUTPUT=$PreCatInput"L1to8_R1a2_001.aln-pe.sorted.bam""_CollectAlignemntSummaryMetrics.txt" \
#REFERENCE_SEQUENCE=$REFERENCE \
#VALIDATION_STRINGENCY=LENIENT
#
#/usr/bin/time -o ./ColInsSizMet.usage -f "%Uuser %Ssystem %Eelapsed %PCPU (%Xtext+%Ddata %Mmax)k" \
#java -Xmx30g -jar /a/apps/picard/latest/dist/picard.jar CollectInsertSizeMetrics \
#VALIDATION_STRINGENCY=LENIENT \
#HISTOGRAM_FILE= $PreCatInput"L1to8_R1a2_001.aln-pe.sorted.bam""_CollectInsertSizeMetrics.pdf" \
#INPUT= $PreCatInput"L1to8_R1a2_001.aln-pe.sorted.bam" \
#OUTPUT= $PreCatInput"L1to8_R1a2_001.aln-pe.sorted.bam""_CollectInsertSizeMetrics.txt"
#

mkdir /data/bam/$PreCatInput
ln -s $PreCatInput"L1to8_R1a2_001.aln-pe.bam" /data/bam/$PreCatInput

#dnaseq workflow
#
#/usr/bin/time -o ./TsTv.usage -f "%Uuser %Ssystem %Eelapsed %PCPU (%Xtext+%Ddata %Mmax)k" sudo /a/apps/vcftools/latest/bin/vcftools --vcf WGSQC20150004.haplotypecaller.snps.indels.vcf --TsTv-summary --out WGSQC20150004.haplotypecaller.snps.indels
#
#sudo /a/apps/vcflib/latest/bin/vcfhethomratio /data/bam/WGSQC20150004/WGSQC20150004.haplotypecaller.snps.indels.vcf
#
