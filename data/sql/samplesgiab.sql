CREATE TABLE IF NOT EXISTS samplesgiab
(
    username            VARCHAR(40) NOT NULL,
    projectname         VARCHAR(40) NOT NULL,
    samplename          VARCHAR(255) NOT NULL,
    subject             VARCHAR(255) NOT NULL,
    ordinal             INT(4) NOT NULL,
    outputdir           VARCHAR(255) NOT NULL,
    sourcebucket        VARCHAR(255) NOT NULL,
    status              VARCHAR(40) NOT NULL,
 
    PRIMARY KEY  ( username, projectname, samplename )
);
