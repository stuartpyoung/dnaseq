CREATE TABLE IF NOT EXISTS sample
(
    username            VARCHAR(40) NOT NULL,
    project             VARCHAR(40) NOT NULL,
    sample              VARCHAR(40) NOT NULL,
    familyid            VARCHAR(40) NOT NULL,
    relationship        VARCHAR(40) NOT NULL,
    status              VARCHAR(40) NOT NULL,
 
    PRIMARY KEY  (username, project, sample, relationship)
)

