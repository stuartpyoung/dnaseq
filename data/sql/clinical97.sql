CREATE TABLE IF NOT EXISTS clinical97
(
    username            VARCHAR(40) NOT NULL,
    project             VARCHAR(40) NOT NULL,
    sample              VARCHAR(255) NOT NULL,
    run                 VARCHAR(255) NOT NULL,
    subject             VARCHAR(255) NOT NULL,
    ordinal             INT(4) NOT NULL,
    outputdir           VARCHAR(255) NOT NULL,
    sourcebucket        VARCHAR(255) NOT NULL,
    targetbucket        VARCHAR(255) NOT NULL,
    status              VARCHAR(40) NOT NULL,
 
    PRIMARY KEY  (username, project, sample)
)
