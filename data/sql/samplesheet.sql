CREATE TABLE IF NOT EXISTS samplesheet
(
    username            VARCHAR(40) NOT NULL,
    projectname         VARCHAR(40) NOT NULL,
    sampleid            VARCHAR(40) NOT NULL,
    groupid             VARCHAR(40) NOT NULL,
    laneid              VARCHAR(40) NOT NULL,
    barcodeid           VARCHAR(40) NOT NULL,
    barcode             VARCHAR(40) NOT NULL,
    status              VARCHAR(40) NOT NULL,
 
    PRIMARY KEY  ( username, projectname, sampleid, laneid )
)

