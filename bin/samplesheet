#!/usr/bin/perl -w

=head2

NAME		loadsamplesheet

PURPOSE

	Load samples from Illumina sequencing run SampleSheet file

INPUT

	1. USERNAME
	2. PROJECT NAME
	3. LOCATION OF TEMPLATE *.sql TABLE FILE
	3. LOCATION OF SampleSheet FILE

OUTPUT

	1. DATABASE TABLE: samplesheet_<USERNAME>_<PROJECTNAME> 
	2. ADD ONE ROW PER SAMPLE TO TABLE

USAGE

	loadsamplesheet <--project String> <--samplesheet String> [--username String] [-h] 

--project	  :   Name of project (e.g., dnaseq)
--samplesheet :   Location of SampleSheet.csv file
--username	  :   Name of user (default: system user account name)
--help        :   print this help message

< option > denotes REQUIRED argument
[ option ] denotes OPTIONAL argument

EXAMPLE

./loadsamplesheet \
--username my.user \
--project dnaseq \
--samplesheet ../t/unit/bin/samplesheet/inputs/SampleSheet.singleindex.csv \
--log 4

=cut

#### EXTERNAL MODULES
# use Data::Dumper;
use Getopt::Long;
use FindBin qw($Bin);

#### INTERNAL MODULES
use lib "$Bin/../lib";
use Conf::Yaml;
use DnaSeq::SampleSheet;

#### SET LOG
my ($script)	=	$0	=~	/([^\.^\/]+)/;
my $logfile 	= "/tmp/$script.$$.log";

#### GET OPTIONS
my $log 		= 	2;
my $printlog 	= 	5;
my $username;
my $project;
my $samplesheet;
my $lanes;
my $help;
GetOptions (
	'username=s' 	=> \$username,
	'project=s' 	=> \$project,
	'samplesheet=s' => \$samplesheet,
	'lanes=s'       => \$lanes,

	'log=i' 		=> \$log,
	'printlog=i' 	=> \$printlog,
	'help' => \$help) or die "No options specified. Try '--help'\n";
if ( defined $help )	{	usage();	}

#### CHECK INPUTS
die "username not defined (option --username)\n" if not defined $username;
die "project not defined (option --project)\n" if not defined $project;

#### GET CONF
my $configfile = "$Bin/../conf/config.yml";
my $conf = Conf::Yaml->new({
	inputfile 	=> $configfile,
	logfile		=>	$logfile,
	log			=>	2,
	printlog	=>	5
});

my $object = DnaSeq::SampleSheet->new({
	conf		=>	$conf,
	configfile	=>	$configfile,
	logfile		=>	$logfile,
	log			=>	$log,
	printlog	=>	$printlog
});

$object->load( $project, $samplesheet, $username, $lanes );

#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#									SUBROUTINES
#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


sub usage
{
    print `perldoc $0`;
	exit;
}
