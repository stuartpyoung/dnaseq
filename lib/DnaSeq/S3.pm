use MooseX::Declare;

use strict;
use warnings;

class DnaSeq::S3 extends DnaSeq::Common {

use FindBin qw($Bin);

use Agua::CLI::App;

use Conf::Yaml;

# Strings
has 'batchsize'		=> 	( isa => 'Int|Undef', is => 'rw', default	=>	5 );

# Strings
has 'sleep'			=> 	( isa => 'Str|Undef', is => 'rw', default	=>	10 );
has 'version'		=> 	( isa => 'Str|Undef', is => 'rw', default	=>	"v1.0.4" );

# Objects
has 'conf'			=> ( isa => 'Conf::Yaml', is => 'rw', lazy => 1, builder => "setConf" );

method download ($username, $bucket, $inputfile, $outputdir) {
	$self->logDebug("bucket", $bucket);
	$self->logDebug("inputfile", $inputfile);
	$self->logDebug("outputdir", $outputdir);
	
	#### SET .boto FILE
	$self->createBotoFile($username);
	
	#### GET EXECUTABLE
	my $installdir	=	$self->getInstallDir("s3lib");
	$self->logDebug("installdir", $installdir);
	my $executable	=	 "$installdir/s3_download.py";

	#### CREATE OUTPUTDIR
	`mkdir -p $outputdir` if not -d $outputdir;
	
	#### CHANGE DIR
	chdir($outputdir);
	
	my $command	= "$executable $bucket $inputfile";
	$self->logDebug("command", $command);
	
	print "Command: $command\n";
		
	`$command`;
}

method upload ($username, $bucket, $inputfile, $keyname) {
	$self->logDebug("bucket", $bucket);
	$self->logDebug("inputfile", $inputfile);
	$self->logDebug("keyname", $keyname);
	
	#### SET .boto FILE
	$self->setBotoFile($username);
	
	#### GET EXECUTABLE
	my $installdir	=	$self->getInstallDir("s3lib");
	$self->logDebug("installdir", $installdir);
	my $executable	=	 "$installdir/s3_upload.py";
	
	my $command	= "$executable $inputfile $bucket $keyname";
	$self->logDebug("command", $command);
	
	print "Command: $command\n";
		
	`$command`;
}

method createBotoFile ($username) {
	my $accesskeyid = $self->conf()->getKey("aws", "AWSACCESSKEYID");
	my $secretaccesskey = $self->conf()->getKey("aws", "AWSSECRETACCESSKEY");
	
my $content = qq{	
[Credentials]
aws_access_key_id=$accesskeyid
aws_secret_access_key=$secretaccesskey
};
	$self->logDebug("content", $content);
	
	my $file = $self->setBotoFile($username);
	$self->logDebug("file", $file);
	
	$self->printToFile($file, $content);
}

method setBotoFile ($username) {
	$self->logDebug("username", $username);

	my $whoami = `whoami`;
	$whoami =~ s/\s+$//g;
	return "/root/.boto" if $whoami eq "root";

	my $userdir = $self->conf()->getKey("agua", "USERDIR");
	$self->logDebug("userdir", $userdir);
	
	return "$userdir/$username/.boto";	
}


	
}

