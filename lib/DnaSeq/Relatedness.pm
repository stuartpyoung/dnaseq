use MooseX::Declare;

use strict;
use warnings;

class DnaSeq::Relatedness extends DnaSeq::Common with (Logger, Agua::Common::Database) {

use FindBin qw($Bin);

#use Agua::CLI::App;
#use Synapse;

#use Conf::Yaml;

# Strings
has 'uuid'			=> 	( isa => 'Str|Undef', is => 'rw');
has 'vcf'			=> 	( isa => 'Str|Undef', is => 'rw');
has 'outfile'		=> 	( isa => 'Str|Undef', is => 'rw');
has 'sampletable'	=> 	( isa => 'Str|Undef', is => 'rw', default	=>	"sample" );

# Objects
has 'conf'			=> ( isa => 'Conf::Yaml', is => 'rw', lazy => 1, builder => "setConf" );
has 'db'			=> ( isa => 'Agua::DBase::MySQL', is => 'rw', required => 0 );
has 'envars'		=> ( isa => 'Conf::Yaml', is => 'rw', lazy => 1, builder => "setEnvars" );

method getRelatedness ($uuid, $vcf, $outfile, $familyfield, $sampletable) {
	$self->logDebug("uuid", $uuid);
	$self->logDebug("vcf", $vcf);
	$self->logDebug("outfile", $outfile);
	$self->logDebug("familyfield", $familyfield);
	$self->logDebug("sampletable", $sampletable);

	print "Returning because data for family members of patient $uuid is not available\n" and return if not $self->familyIsComplete($uuid);
	
	my $familyid = $self->getFamilyId($uuid, $familyfield, $sampletable);
	my $family	=	$self->getFamily($sampletable, $familyid);
	$self->logDebug("family", $family);
	my $uuids = [];
	foreach my $member ( @$family ) {
		push @$uuids, $member->{sample};
	}
	$self->logDebug("uuids", $uuids);
	
	$self->logDebug("DOING self->runCommand()");
	return $self->runCommand($uuids, $vcf, $outfile);
}

method runCommand ($uuids, $vcf, $outfile) {
	$self->logDebug("uuids", $uuids);
	$self->logDebug("vcf", $vcf);
	$self->logDebug("outfile", $outfile);
	
	#### GET EXECUTABLE
	my $installdir	=	$self->getInstallDir("vcftools");
	$self->logDebug("installdir", $installdir);
	my $executable	=	 "$installdir/bin/freebayes";
	$self->logDebug("executable", $executable);

	my $command	= qq{$executable --relatedness2 \\
--vcf $vcf \\
--out $outfile};
		
	print "$command\n";
	print `$command`;

	$self->logDebug("Error occurred with vcftools relatedness2 calculation") and return 0 if $@;	

	return 1;
}

method familyIsComplete ($uuid, $familyfield, $sampletable) {
	$self->logDebug("uuid", $uuid);
	$self->logDebug("familyfield", $familyfield);
	$self->logDebug("sampletable", $sampletable);

	#### SET DATABASE HANDLE
	$self->setDbh() if not defined $self->db();
	
	#### GET SAMPLE HASH
	my $familyid	=	$self->getFamilyId($uuid, $familyfield, $sampletable);
	$self->logDebug("familyid", $familyid);

	my $family		=	$self->getFamily($sampletable, $familyid);
	$self->logDebug("family", $family);
	print "No family for uuid: $uuid\n" and return if not defined $family;	
	
	foreach my $member ( @$family ) {
		$self->logDebug("member->{sample}", $member->{sample});
		next if $member->{sample} eq $uuid;

		my $query = qq{SELECT status FROM provenance
WHERE sample='$member->{sample}'};
		my $status = $self->db()->query($query);
		$self->logDebug("query", $query);
		return 0 if $status ne "completed";
	}
	
	return 1;
}

method getFamilyId ($sample, $familyfield, $sampletable) {
	my $query		=	qq{SELECT $familyfield FROM $sampletable
WHERE sample='$sample'};
	$self->logDebug("query", $query);
	my $samplehash	=	$self->db()->query($query);
	$self->logDebug("samplehash", $samplehash);
	
	return $samplehash;
}

method getFamily ($table, $familyid) {
	$self->logDebug("familyid", $familyid);
	my $query		=	qq{SELECT * FROM $table
WHERE familyid='$familyid'
ORDER BY sample};
	$self->logDebug("query", $query);

	my $samplehash	=	$self->db()->queryhasharray($query);
	$self->logDebug("samplehash", $samplehash);
	
	return $samplehash;
}


	
}

