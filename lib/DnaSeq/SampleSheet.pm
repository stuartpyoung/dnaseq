use MooseX::Declare;
use strict;
use warnings;

class DnaSeq::SampleSheet with Util::Logger {

use FindBin qw($Bin);
use Table::Main;
use Conf::Yaml;

has 'conf' 			=> (
	is =>	'rw',
	isa => 'Conf::Yaml'
);

has 'table'		=>	(
	is 			=>	'rw',
	isa 		=>	'Table::Main',
	# lazy		=>	1,
	# builder	=>	"setTable"
);

method setTable {
	my $table = Table::Main->new({
		dbfile    => 	$self->conf()->getKey("core:INSTALLDIR") . "/" . $self->conf()->getKey("database:DBFILE"),
		conf			=>	$self->conf(),
		log				=>	$self->log(),
		printlog	=>	$self->printlog()
	});

	$self->table($table);	
}

method BUILD ($hash) {
	$self->logDebug("hash", $hash);
	my $args = $hash->{args};
	foreach my $key ( keys %$args ) {
		if ( defined $args->{$key} and $self->can($key) ) {
			$self->logDebug("$key: $args->{$key}");
			$self->$key($args->{$key});
		}
	}

	#### SET DATABASE
	$self->setTable();
}

method fileContents ($file) {
    $self->logDebug("file", $file);
    die("file not defined\n") if not defined $file;
    die("Can't find file: $file\n$!") if not -f $file;

    my $temp = $/;
    $/ = undef;
    open(FILE, $file) or die("Can't open file: $file\n$!");
    my $contents = <FILE>;
    close(FILE);
    $/ = $temp;
    
    return $contents;
}

method setUsername ( $username ) {
	$self->logDebug( "username", $username );

	if ( defined $username and $username ne "" ) {
		return $username;
	}

	my $whoami    =   `whoami`;
	$whoami       =~  s/\s+$//;
	$self->logDebug("whoami", $whoami);

	return $whoami;
}

method load ( $projectname, $samplesheet, $username, $lanes ) {
	# 1. CREATE NEW TABLE: samplesheet_<USERNAME>_<PROJECTNAME>
	# 2. CREATE INPUT .tsv FILE FROM SampleSheet.csv
	# 3. LOAD .tsv FILE INTO TABLE
	# 4. ADD NEW TABLE INFO TO sampletable TABLE

	$username = $self->setUsername($username);
	$self->logDebug("username", $username);
	$self->logDebug("projectname", $projectname);
	$self->logDebug("samplesheet", $samplesheet);
	$self->logDebug("lanes", $lanes);

	#### CHECK VALUES
	$self->logError("username not defined") and return if not defined $username;
	$self->logError("projectname not defined") and return if not defined $projectname;
	$self->logError("samplesheet not defined") and return if not defined $samplesheet;
	$self->logError("lanes not defined") and return if not defined $lanes;
	
	my $laneids = [ split ",", $lanes ];
	$self->logDebug("laneids", $laneids);

	# 1. CREATE NEW TABLE: samplesheet_<USERNAME>_<PROJECTNAME>
	my $table = "samplesheet_" . $username . "_" . $projectname;
	$table =~ s/[\.\-]+//g;
	my $sqlfile = "$Bin/../data/sql/samplesheet.sql";
	$self->logDebug("table", $table);
	$self->logError("Can't find sqlfile: $sqlfile") and return if not -f $sqlfile;
	$self->logDebug("sqlfile", $sqlfile);
	
	#### DROP TABLE IF ALREADY EXISTS
	my $query	=	"DROP TABLE $table";
	if ( $self->table()->db()->hasTable( $table ) ) {
		$self->logDebug("query", $query);
		$self->table()->db()->do($query);
	}
	
	#### CREATE TABLE
	$query	=	$self->fileContents($sqlfile);
	$self->logDebug("query", $query);
	$query =~ s/(EXISTS\s+)\S+/EXISTS $table/;
	$self->logDebug("query", $query);
	$self->logDebug("self->table()", $self->table());
	$self->table()->db()->do($query);
	
	# 2. CREATE INPUT .tsv FILE FROM SampleSheet.csv
	my $tsvfile = "$Bin/../data/tsv/samplesheet-$username-$projectname.tsv";
	$self->logDebug("tsvfile", $tsvfile);
	$self->samplesheetToTsv( $username, $projectname, $samplesheet, $laneids, $tsvfile );

	# 3. LOAD .tsv FILE INTO TABLE
	my $success	=	$self->loadTsvFile( $table, $tsvfile );
	$self->logDebug("success", $success);

	# 4. ADD NEW TABLE INFO TO samples TABLE
	$success = $self->addToSampletable( $username, $projectname, $table );
    	
	return $success;	
}

method addToSampletable ( $username, $projectname, $table ) {
	$self->logDebug("username", $username);
	$self->logDebug("projectname", $projectname);
	$self->logDebug("table", $table);

	if ( $self->table()->db()->hasTable($table) ) {
		my $query	=	qq{SELECT 1 FROM sampletable
	WHERE username='$username'
	AND projectname='$projectname'
	AND sampletable='$table'};
		$self->logDebug("query", $query);
		my $exists = $self->table()->db()->query($query);
		$self->logDebug("exists", $exists);
		
		return if $exists;
  }

	my $query	=	qq{INSERT INTO sampletable VALUES
	('$username', '$projectname', '$table')};
	$self->logDebug("query", $query);
	my $success	=	$self->table()->db()->do($query);
	$self->logDebug("success", $success);

	return $success;
}

method samplesheetToTsv ($username, $projectname, $samplesheet, $laneids, $tsvfile) {
	$self->logDebug("username", $username);
	$self->logDebug("projectname", $projectname);
	$self->logDebug("tsvfile", $tsvfile);
	
	my $status = "none";
	my $contents		=	$self->fileContents( $samplesheet );
	$contents =~ s/^.+\[Data\].*?\n//ms;
	$self->logDebug("contents", $contents);
	my $lines = [ split "\n", $contents ];
	shift @$lines;
	my $outputs	=	[];
	my $groupcounter = 0;
	foreach my $line ( @$lines ) {
		next if $line =~ /^\s*sample\s+/;
		next if $line =~ /^#/;

		$groupcounter++;
		my $groupid = "S$groupcounter" ;
		
		my $tokens = [ split ",", $line ];
		my $sampleid = $$tokens[ 0 ];
		
		my $barcodeid = $$tokens[ 4 ];
		my $barcode = $$tokens[ 5 ];

		foreach my $laneid ( @$laneids ) {
			push @$outputs,	"$username\t$projectname\t$sampleid\t$groupid\t$laneid\t$barcodeid\t$barcode\t$status\n";
		}
	}
	
	open(OUT, ">", $tsvfile) or die "Can't open tsvfile: $tsvfile\n";
	foreach my $output ( @$outputs ) {
		print OUT $output;
	}
	close(OUT) or die "Can't close tsvfile: $tsvfile\n";

	return $tsvfile;
}

method loadTsvFile ($table, $file) {
	$self->logDebug("table", $table);
	$self->logDebug("file", $file);
	
	#### DON'T USE 'LOAD DATA LOCAL INFILE' (MYSQL ONLY)
	my $success = $self->table()->db()->importFile($table, $file);
	$self->logCritical("load data failed") if not $success;
	
	return $success;	
}

method fileContents ($file) {
    $self->logDebug("file", $file);
    die("file not defined\n") if not defined $file;
    die("Can't find file: $file\n$!") if not -f $file;

    my $temp = $/;
    $/ = undef;
    open(FILE, $file) or die("Can't open file: $file\n$!");
    my $contents = <FILE>;
    close(FILE);
    $/ = $temp;
    
    return $contents;
}

method backupFile ($inputfile) {
	$self->logDebug("inputfile", $inputfile);
	my $backup = "$inputfile.bkp";
	`cp $inputfile $backup` if not -f $backup;
	
	return $inputfile;
}

	
}

