package FastQC;

use Moose::Role;
use Moose::Util::TypeConstraints;
use Method::Signatures::Simple;

with 'Common';

=head2

PACKAGE		Logic::Fork

PURPOSE

    1. SELECT ONE OR MORE WORKFLOW PATHS BASED ON ONE OR BOTH OF THE FOLLOWING:
    
        - CONTENTS OF INPUT FILE (USING --inputfile OPTION)
    
        - CONTENTS OF DATABASE AND OTHER DATA (USING --modfile OPTION)

    2. SELECT BRANCH BY:

        - INDEX FOUND IN FIRST LINE OF INPUT FILE (DEFAULT, USES --inputfile OPTION)

        - INSTRUCTIONS LOADED DYNAMICALLY FROM *.pm FILE (--modfile OPTION)

        - STAGES AS ARGUMENTS:
        
            --if
            --elsif
            --else
            
        
        - MULTIPLE --regex AND --branch OPTIONS
        
            WHERE --branch IS A STRING OF FORMAT 'Project:Workflow:StageNumber:StageName'

    
=cut

use strict;
use warnings;

method getBlocks ($datafile, $metrics) {
    $self->logDebug("datafile", $datafile);
    $self->logDebug("metrics", $metrics);
    
    my $blocks = {};
    my $contents	=	$self->getFileContents($datafile);
    $contents       =~ s/^.+?\n//;
    my $textblocks   =   [ split ">>END_MODULE", $contents ];
    #shift @$textblocks;
    $self->logDebug("textblocks", $textblocks);
    foreach my $block ( @$textblocks ) {
        $block =~ />>([^\n]+)\s+(\S+)\n(.+)\n$/ms;
        my $key = $1;
        my $text = $3;
        #$self->logDebug("key", $key);
        #$self->logDebug("text", $text);
        
        if ( exists $metrics->{$key} ) {
           $blocks->{$key} = $text;
        }
    }
    
    return $blocks;
}

method getColumnIndexes ($block) {
    $self->logDebug("block", $block);
    my $lines = [ split "\n", $block ];
    my $header  =   shift @$lines;
    $header =~ s/^#//;
    
    return $self->parseColumnHeader($header);
}

method selectIndex ($inputfiles) {
	$self->logDebug("inputfiles", $inputfiles);

    my $index = 0;
    my $files = [ split ",", $inputfiles ];
    $self->logDebug("files", $files);
    my $datafile = $$files[0];
    my $parameterfile = $$files[1];
    
    my $metrics = $self->getMetrics($parameterfile);
    $self->logDebug("metrics", $metrics);
    
    my $blocks = $self->getBlocks($datafile, $metrics);
    
    my $results = [];
    my @keys = keys %$metrics;
    @keys = sort @keys;
    foreach my $key ( @keys ) {
        my $block = $blocks->{$key};
        $self->logDebug("key", $key);

        my $columnindexes = $self->getColumnIndexes($block);
        $self->logDebug("columnindexes", $columnindexes);

        my $subroutine = $self->getSubroutine($key);
        $self->logDebug("subroutine", $subroutine);
        my $rules = $metrics->{$key};

        #### GENERIC CALL TO SUBROUTINE
        my ($currentindex, $currentresults) = $self->$subroutine($columnindexes, $block, $rules);
        print "$key current $currentindex index $index\n";

        @$results = ( @$results, @$currentresults);
        $index = $currentindex if $currentindex > $index;
    }
    
    return $index, $results;
}

=doc

    SUBROUTINE checkPerBaseSequenceQuality
    
    PURPOSE
    
        1. RETURN 0 IF NO WARNINGS OR FAILURES

        1. RETURN 1 IF WARNINGS AND NO FAILURES
        
        2. RETURN 2 IF FAILURES
        
    INPUTS
    
        1. TSV parameter FILE
        
        2. METRICS HASH (SEE getMetrics METHOD)
        
    OUTPUTS
    
        1. 0 FOR PASS, 1 FOR WARNING, 2 FOR FAILURE

        2. ARRAY CONTAINING REPORT FOR EACH TESTED
        
            METRIC IN THE FORM:
            
                [
                    index   =>  0,
                    result  => {
                    {
                       metric       =>  "warning",
                       rulename     =>  "averageQuality",
                       outcome      =>  "failure",
                       threshold    =>  0.8,
                       actual       =>  0.9,
                       result       =>  "pass"
                    },
                    index   =>  1,
                    result  => {
                       rulename     =>  "averageQuality",
                       outcome      =>  "warning",
                       threshold    =>  1.0,
                       actual       =>  0.9,
                       result       =>  "fail"
                    },
                    ...
                ]
        
=cut
#### Per base sequence quality
method checkPerBaseSequenceQuality  ($columnindexes, $block, $rules) {
    $self->logDebug("columnindexes", $columnindexes);
    #$self->logDebug("block", $block);
    $self->logDebug("rules", $rules);
    
    my $sub = sub {
        my $threshold   =   shift;
        my $actual      =   shift;
        my $outcome     =   shift;
        my $index       =   shift;

        my $result      =   "pass";
        if ( $threshold > $actual ) {
            $result     =   "fail";            
            if ( $outcome eq "warning" ) {
                $index = 1 if $index < 1;
            }
            elsif ( $outcome eq "failure" ) {
                $index = 2;
            }
        }
        
        return $index, $result;
    };
    
    my $metric = "Per Base Sequence Quality";
    return $self->checkColumnRules($columnindexes, $block, $rules, $sub, $metric);
}

#### Per sequence quality scores
method checkPerSequenceQualityScores  ($columnindexes, $block, $rules) {
    $self->logDebug("block", $block);
    $self->logDebug("rules", $rules);
    my $index = 0;
    my $lines = [ split "\n", $block ];
    shift @$lines;
    $self->logDebug("lines", $lines);
    
    #### GET MEAN
    my $columnindex = 1;
    my $mean = $self->getQualityMean($lines);
    
    #### ITERATE OVER ALL RULES FOR ALL LINES
    my $results =   [];
    foreach my $rule ( @$rules ) {
        $self->logDebug("rule", $rule);
        my $outcome     =   $$rule[0];
        my $rulename    =   $$rule[1];
        my $threshold   =   $$rule[2];
        $self->logDebug("rulename", $rulename);
        $self->logDebug("outcome", $outcome);
        $self->logDebug("threshold", $threshold);
    
        my $result = "pass";
        if ( $threshold > $mean ) {
            $result = "fail";
            if ( $outcome eq "warning" ) {
                $index = 1 if $index < 1;
            }
            elsif ( $outcome eq "failure" ) {
                $index = 2;
            }
        }

        push @$results, {
            index   =>  $index,
            result  =>  {
                metric      =>  "Per Sequence Quality Scores",
                rulename    =>  $rulename,
                outcome     =>  $outcome,
                threshold   =>  $threshold,
                actual      =>  $mean,
                result      =>  $result
            }
        };
    }
    
    return $index, $results;
}

method getQualityMean ($lines) {
    $self->logDebug("lines", $lines);
    my $mean        =   0;
    my $reads       =   0;
    foreach my $line ( @$lines ) {
        my @elements = split "\t", $line;
        my $quality  =   $elements[0];
        my $count   =   $elements[1];
        $mean += $quality * $count;
        $reads += $count;
    }
    $mean = $mean / $reads;
    $self->logDebug("mean", $mean);
        
    return $mean;
    
}

method getMean ($lines, $columnindex) {
    my $mean = 0;
    foreach my $line ( @$lines ) {
        my @elements = split "\t", $line;
        $mean += $elements[$columnindex]; 
    }
    $mean = $mean / scalar(@$lines);
    $self->logDebug("mean", $mean);
        
    return $mean;
}

#### Per base sequence content
method checkPerBaseSequenceContent  ($columnindexes, $block, $rules) {
    $self->logDebug("block", $block);
    $self->logDebug("rules", $rules);
    my $index = 0;
    
    my $averages = $self->getBaseAverages($block);
    my $difference  = $self->getBaseDifference($averages);
    $self->logDebug("difference", $difference);
    
    #### ITERATE OVER ALL RULES FOR ALL LINES
    my $results =   [];
    foreach my $rule ( @$rules ) {
        $self->logDebug("rule", $rule);
        my $outcome =       $$rule[0];
        my $rulename    =   $$rule[1];
        my $threshold       =   $$rule[2];
        $self->logDebug("rulename", $rulename);
        $self->logDebug("outcome", $outcome);
        $self->logDebug("threshold", $threshold);
    
        my $result = "pass";
        if ( $difference > $threshold ) {
            $result = "fail";
            if ( $outcome eq "warning" ) {
                #$self->logDebug("WARNING");
                $index = 1 if $index < 1;
            }
            elsif ( $outcome eq "failure" ) {
                #$self->logDebug("FAILURE");
                $index = 2;
            }
        }

        push @$results, {
            index   =>  $index,
            result  =>  {
                metric      =>  "Per Base Sequence Content",
                rulename    =>  $rulename,
                outcome     =>  $outcome,
                threshold   =>  $threshold,
                actual      =>  $difference,
                result      =>  $result
            }
        };
    }
    
    return $index, $results;
}

method getBaseAverages ($block) {
    #$self->logDebug("block", $block);
    
    my $lines = [ split "\n", $block ];
    #$self->logDebug("lines", $lines);

    #### GET COLUMN INDEXES
    my $header  =   shift @$lines;
    my $columnindexes =   $self->parseColumnHeader($header);    
    my $averages = {
        A   =>  0,
        T   =>  0,
        G   =>  0,
        C   =>  0
    };
    
    #### GET VALUE COUNTS
    my $totalentries = 0;
    ($averages, $totalentries) = $self->getColumnCounts($lines, $columnindexes, $averages);
    $self->logDebug("totalentries", $totalentries);
    
    my $totalcounts = 0;
    foreach my $key ( keys %$averages ) {
        $totalcounts += $averages->{$key};
    }
    $self->logDebug("totalcounts", $totalcounts);
    
    #### SET VALUES TO PERCENTAGES    
    foreach my $key ( keys %$averages ) {
        $averages->{$key} = ($averages->{$key} / $totalcounts) * 100;
    }
    $self->logDebug("FINAL averages", $averages);

    return $averages;
}

method getColumnCounts ($lines, $columnindexes, $averages) {
    $self->logDebug("columnindexes", $columnindexes);
    $self->logDebug("averages", $averages);
    
    #my $min =   undef;
    #my $max =   undef;
    my $total = 0;
    for ( my $linecounter = 0; $linecounter < scalar(@$lines); $linecounter++) {
        my $line = $$lines[$linecounter];
        my $elements = [ split "\t", $line ];
        #$self->logDebug("elements", $elements);
        my $number = $$elements[0];
        
        foreach my $key ( keys %{$columnindexes} ) {
            next if $key eq "#Base";
            my $columnindex   =   $columnindexes->{$key};
            my $value = $$elements[$columnindex];
            #$self->logDebug("key $key index $columnindex number $number value", $value);

            if ( $number =~ /^\d+$/ ) {
                #$min = $number if not defined $min;
                #$max = $number if $linecounter == scalar(@$lines) - 1;
                $averages->{$key} += $value;
                $total += 1;
            }
            else {
                my ($start, $stop)  =   $number =~ /^(\d+)-(\d+)$/;
                #$self->logDebug("start", $start);
                #$self->logDebug("stop", $stop);
                #$min = $start if not defined $min;
                #$max = $stop if $linecounter == scalar(@$lines) - 1;
                $averages->{$key} += $value * ($stop - $start + 1);
                $total += $stop - $start + 1;
                #$self->logDebug("averages->{$key}", $averages->{$key});
            }
            #$self->logDebug("total", $total);
        }
    }
    $self->logDebug("total", $total);
    $self->logDebug("FINAL averages", $averages);

    return $averages, $total;
}

method getBaseDifference ($averages) {
    my $difference = 0;
    my $keys = [ keys %$averages ];
    for ( my $i = 0; $i < @$keys; $i++ ) {
        my $key1 = $$keys[$i];
        for ( my $k = $i + 1; $k < scalar(@$keys); $k++ ) {
            my $key2 = $$keys[$k];
            my $diff = abs($averages->{$key1} - $averages->{$key2});
            $self->logDebug("diff", $diff);
            
            $difference = $diff if $difference < $diff;
        }
    }
    $self->logDebug("difference", $difference);
    
    return $difference;
}

#### Per sequence GC content
=doc

=head2 SUBROUTINE   checkPerBaseGCContent

=head2 PURPOSE

    VALIDATE DATA USING FASTQC OUTPUT AND USER-INPUT THRESHOLDS

    RELEVANT FASTQC WEBSITE PAGE:

        http://www.bioinformatics.babraham.ac.uk/projects/fastqc/Help/3%20Analysis%20Modules/5%20Per%20Sequence%20GC%20Content.html
    
    
=head2  NOTES

    
    APPROXIMATION OF NORMAL DISTRIBUTION:

        http://www.johndcook.com/blog/2010/04/29/simple-approximation-to-normal-distribution/

        F(x) = 1/(1 + exp(-0.07056 x3 – 1.5976 x))
       
=cut
method checkPerSequenceGCContent  ($columnindexes, $block, $rules) {
    $self->logDebug("columnindexes", $columnindexes);
    $self->logDebug("rules", $rules);

    my $index = 0;
    
    #### GET COLUMN INDEXES
    my $lines   =   [ split "\n", $block ];
    my $header  =   shift @$lines;
    $self->logDebug("header", $header);
    
    my $averages = {
        "%GC"   =>  0
    };
    
    my $total = 0;
    ($averages, $total) = $self->getColumnCounts($lines, $columnindexes, $averages);
    my $average = $averages->{"%GC"} / $total;
    $self->logDebug("FINAL total", $total);
    $self->logDebug("FINAL averages", $averages);
    $self->logDebug("average", $average);
    
    my $deviation = $self->getDeviation($block, $average, $columnindexes);
    $self->logDebug("deviation", $deviation);
    $self->logDebug("average", $average);
    
    my $counts = $self->getGCCounts($block);
    $self->logDebug("counts", $counts);
    
    #my $difference  =   $self->getDifferenceFromNormal();
    

    ##### ITERATE OVER ALL RULES FOR ALL LINES
    #foreach my $rule ( @$rules ) {
    #    $self->logDebug("rule", $rule);
    #    my $outcome =       $$rule[0];
    #    my $rulename    =   $$rule[1];
    #    my $threshold       =   $$rule[2];
    #    $self->logDebug("rulename", $rulename);
    #    $self->logDebug("outcome", $outcome);
    #    $self->logDebug("threshold", $threshold);
    #
    #    if ( $average > $threshold ) {
    #        if ( $outcome eq "warning" ) {
    #            $self->logDebug("WARNING");
    #            $index = 1 if $index < 1;
    #        }
    #        elsif ( $outcome eq "failure" ) {
    #            $self->logDebug("FAILURE");
    #            $index = 2;
    #        }
    #    }
    #}
    
    return $index;
}

method getDifferenceFromNormal() {
    
}

method getGCCounts ($block) {
    my $array = [];
    my $lines = [ split "\n", $block ];
    shift @$lines;
    foreach my $line ( @$lines ) {
        push @$array, $line =~ /(\S+)$/;
    }
    
    return $array;    
}

method getDeviation ($block, $average, $columnindexes) {
	my $variance = 0;
    my $lines = [ split "\n", $block ];
    
    my $count = 0;        
    my $totalvariance = 0;
    for ( my $linecounter = 0; $linecounter < scalar(@$lines); $linecounter++) {
        my $line = $$lines[$linecounter];
        my $elements = [ split "\t", $line ];
        my $number = $$elements[0];
        next if $number =~ /^#Base/;

        my $columnindex   =   $columnindexes->{"%GC"};
        $self->logDebug("columnindex", $columnindex);
        my $value = $$elements[$columnindex];
        $self->logDebug("columnindex $columnindex number $number value", $value);

        if ( $number =~ /^\d+$/ ) {
            $totalvariance += ($value - $average)**2;
            
            $self->logDebug("variance", ($value - $average)**2);
            $count++;
        }
        else {
            my ($start, $stop)  =   $number =~ /^(\d+)-(\d+)$/;
            #$self->logDebug("start", $start);
            #$self->logDebug("stop", $stop);
            for ( my $i = $start; $i < $stop + 1; $i++ ) {
                $totalvariance += ($i - $average)**2;
                $self->logDebug("variance", ($value - $average)**2);
                $count++;
            }
        }
    }
    $self->logDebug("count", $count);
    $self->logDebug("totalvariance", $totalvariance);
	$totalvariance = $totalvariance / ($count - 1);
    $self->logDebug("FINAL totalvariance", $totalvariance);
	my $deviation = $totalvariance**0.5;
    $self->logDebug("deviation", $deviation);

    return $deviation;
}

#### Per base N content
method checkPerBaseNContent  ($columnindexes, $block, $rules) {
    $self->logDebug("block", $block);
    $self->logDebug("rules", $rules);
    
    my $sub = sub {
        my $threshold   =   shift;
        my $actual      =   shift;
        my $outcome     =   shift;
        my $index       =   shift;
        
        my $result = "pass";
        if ( $actual > $threshold ) {
            $result = "fail";
            if ( $outcome eq "warning" ) {
                $index = 1 if $index < 1;
            }
            elsif ( $outcome eq "failure" ) {
                $index = 2;
            }
        }

        return $index, $result;
    };
    
    my $metric = "Per Base N Content";
    return $self->checkColumnRules($columnindexes, $block, $rules, $sub, $metric);
}

#### Sequence Length Distribution
#### SKIP

#### Sequence Duplication Levels
method checkSequenceDuplicationLevels  ($columnindexes, $block, $rules) {
    my ($duplication)  = $block =~ /Total Duplicate Percentage\s+(\S+)/;
    $self->logDebug("duplication", $duplication);
    
    #### ITERATE OVER ALL RULES FOR ALL LINES
    my $results =   [];
    my $index = 0;
    foreach my $rule ( @$rules ) {
        $self->logDebug("rule", $rule);
        my $outcome =       $$rule[0];
        my $rulename    =   $$rule[1];
        my $threshold       =   $$rule[2];
        $self->logDebug("rulename", $rulename);
        $self->logDebug("outcome", $outcome);
        $self->logDebug("threshold", $threshold);
    
        my $result = "pass";
        if ( $duplication > $threshold ) {
            $result = "fail";
            if ( $outcome eq "warning" ) {
                $index = 1 if $index < 1;
            }
            elsif ( $outcome eq "failure" ) {
                $index = 2;
            }
        }

        push @$results, {
            index   =>  $index,
            result  =>  {
                metric      =>  "Sequence Duplication Levels",
                rulename    =>  $rulename,
                outcome     =>  $outcome,
                threshold   =>  $threshold,
                actual      =>  $duplication,
                result      =>  $result
            }
        };
    }
    
    return $index, $results;
}

#### Overrepresented sequences
method checkOverrepresentedSequences  ($columnindexes, $block, $rules) {
    $self->logDebug("block", $block);
    $self->logDebug("rules", $rules);
    $self->logDebug("columnindexes", $columnindexes);

    my $index = 0;
    
    my $sub = sub {
        my $threshold   =   shift;
        my $actual      =   shift;
        my $outcome     =   shift;
        my $index       =   shift;
        
        my $result = "pass";
        if ( $actual > $threshold ) {
            $result = "fail";
            if ( $outcome eq "warning" ) {
                $index = 1 if $index < 1;
            }
            elsif ( $outcome eq "failure" ) {
                $index = 2;
            }
        }
        
        return $index, $result;
    };
    
    my $metric =    "Overrepresented sequences";
    return $self->checkColumnRules($columnindexes, $block, $rules, $sub, $metric);
}


1;
