package Relatedness2;

use Moose::Role;
use Moose::Util::TypeConstraints;
use Method::Signatures::Simple;

=head2

PACKAGE		Logic::Fork

PURPOSE

    1. SELECT ONE OR MORE WORKFLOW PATHS BASED ON ONE OR BOTH OF THE FOLLOWING:
    
        - CONTENTS OF INPUT FILE (USING --inputfile OPTION)
    
        - CONTENTS OF DATABASE AND OTHER DATA (USING --modfile OPTION)

    2. SELECT BRANCH BY:

        - INDEX FOUND IN FIRST LINE OF INPUT FILE (DEFAULT, USES --inputfile OPTION)

        - INSTRUCTIONS LOADED DYNAMICALLY FROM *.pm FILE (--modfile OPTION)

        - STAGES AS ARGUMENTS:
        
            --if
            --elsif
            --else
            
        
        - MULTIPLE --regex AND --branch OPTIONS
        
            WHERE --branch IS A STRING OF FORMAT 'Project:Workflow:StageNumber:StageName'

    
=cut

use strict;
use warnings;


## Ints
#has 'log'		=>  ( isa => 'Int', is => 'rw', default => 4 );
#has 'printlog'	=>  ( isa => 'Int', is => 'rw' );


#####/////}
#
#method BUILD ($hash) {
#	$self->logDebug("");
#	#$self->logDebug("self", $self);
#	$self->initialise($hash);
#}
#
#method initialise ($hash) {	
#	$self->logDebug("");
#    
#	#### SET CONF LOG
#	$self->conf()->log($self->log());
#	$self->conf()->printlog($self->printlog());	
#}
#
#
method selectIndex ($inputfiles) {
    print "HERE\n";
	$self->logDebug("inputfiles", $inputfiles);

    my $index = 2;

    my $files = [ split ",", $inputfiles ];
    $self->logDebug("files", $files);
    my $datafile = $$files[0];
    my $parameterfile = $$files[1];
    
	#my $contents	=	$self->getFileContents($inputfiles);


    my $metrics = $self->getMetrics($parameterfile);
    my $blocks = $self->getBlocks($datafile, $metrics);

    foreach my $key ( %$metrics ) {
        my $block = $blocks->{$key};
        my $subroutine = $key;
        # BEFORE Per base sequence quality
        $subroutine =~ s/\s+//g;
        # AFTER Perbasesequencequality
        $subroutine = "check" . $subroutine;
        $self->logDebug("subroutine", $subroutine);
        my $currentindex = $self->$subroutine($block);
        
        $index = $currentindex if $currentindex < $index;
        
    }
    
    return $index;    
}

method getMetrics ($parameterfile) {
    my $contents = $self->getContents($parameterfile);
    
    
}

=doc

    SUBROUTINE checkPerbasesequencequality
    
    PURPOSE
    
        1. RETURN 0 IF ANY 'failure' CHECK FAILS
        
        2. RETURN 1 IF ANY 'warning' CHECK FAILS
        
    INPUTS
    
        1. TSV parameter FILE
        
    OUTPUTS
    
        1. RETURN 1 FOR WARNING, 0 FOR FAILURE
        
=cut

method checkPerbasesequencequality ($block) {
    
    return 
}

method checkBasestatistics ($block) {
    
    return 
}

sub getFileContents {
	my $self		=	shift;
	my $file		=	shift;
	
	$self->logNote("file", $file);
	open(FILE, $file) or $self->logCritical("Can't open file: $file") and exit;
	my $temp = $/;
	$/ = undef;
	my $contents = 	<FILE>;
	close(FILE);
	$/ = $temp;

	return $contents;
}





1;
