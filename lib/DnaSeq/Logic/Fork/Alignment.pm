package Alignment;
use Moose::Role;
use Moose::Util::TypeConstraints;
use Method::Signatures::Simple;

with 'Common';

=head2


PACKAGE		Logic::Fork::AlignmentSummary

PURPOSE

    1. SELECT ONE OR MORE WORKFLOW PATHS BASED ON ONE OR BOTH OF THE FOLLOWING:
    
        - CONTENTS OF INPUT FILE (USING --inputfile OPTION)
    
        - CONTENTS OF DATABASE AND OTHER DATA (USING --modfile OPTION)

    2. SELECT BRANCH BY:

        - INDEX FOUND IN FIRST LINE OF INPUT FILE (DEFAULT, USES --inputfile OPTION)

        - INSTRUCTIONS LOADED DYNAMICALLY FROM *.pm FILE (--modfile OPTION)

        - STAGES AS ARGUMENTS:
        
            --if
            --elsif
            --else
            
=cut

use strict;
use warnings;

method selectIndex ($inputfiles) {
	$self->logDebug("inputfiles", $inputfiles);

    my $index = 0;

    my $files = [ split ",", $inputfiles ];
    $self->logDebug("files", $files);
    my $datafile = $$files[0];
    my $parameterfile = $$files[1];
    
    my $metrics = $self->getMetrics($parameterfile);
    $self->logDebug("metrics", $metrics);
    
    no warnings;
    my $blocks = $self->getBlocks($datafile, $metrics);
    my $columnindexes = $self->getColumnIndexes($datafile);
    $self->logDebug("columnindexes", $columnindexes);
    use warnings;
    
    my $sub = sub {
        my $threshold   =   shift;
        my $actual      =   shift;
        my $outcome     =   shift;
        my $index       =   shift;
        my $operator    =   shift;
        #print "SUBROUTINE operator: $operator\n";
        
        my $result      =   "pass";
        if ( $operator eq "max" and $actual > $threshold ) {
            $result     =   "fail";            
        }
        elsif ( $operator eq "min" and $actual < $threshold ) {
            $result     =   "fail";            
        }
        
        if ( $result eq "fail" ) {
            if ( $outcome eq "warning" ) {
                $index = 1 if $index < 1;
            }
            elsif ( $outcome eq "failure" ) {
                $index = 2;
            }
        }
        
        return $index, $result;
    };
        
    my $results = [];
    foreach my $block ( @$blocks ) {
        $self->logDebug("block", $block);
        
        next if $block !~ /PAIR/;
        my ($blockname) = $block =~ /^(\S+)/;
        
        $block = "####\n" . $block;
        
        my @keys = keys %$metrics;
        @keys = sort @keys;
        foreach my $key ( @keys ) {
            my $rules = $metrics->{$key};
            my $metric = "Alignment Summary $blockname";
            my ($currentindex, $currentresults) = $self->checkColumnRules($columnindexes, $block, $rules, $sub, $metric);

            @$results = ( @$results, @$currentresults);
            $index = $currentindex if $index < $currentindex;
        }
    }
    
    return $index, $results;
}

method getBlocks ($datafile, $metrics) {
    $self->logDebug("datafile", $datafile);
    
    my $contents	=	$self->getFileContents($datafile);
    $self->logDebug("contents", $contents);

    my $keys = [ "FIRST_OF_PAIR", "SECOND_OF_PAIR", "PAIR", "UNPAIRED" ];
    my $blocks = [];
    foreach my $key ( @$keys ) {
        my ($block) = $contents =~ /\n($key\s+[^\n]+)/ms;
        push @$blocks, $block;
    }
    $self->logDebug("blocks", $blocks);
    
    return $blocks;
}

method getColumnIndexes ($datafile) {
    #### GET RULE TARGET COLUMN INDEX
    my $contents = $self->getFileContents($datafile);
    $contents =~ s/^.+?\n## METRICS CLASS[^\n]+\n//ms;
    #$self->logDebug("contents", $contents);
    my $lines = [ split "\n", $contents ];
    my $header  =   shift @$lines;
    my $elements=   [ split "\t", $header ];
    #$self->logDebug("elements", $elements);
    my $counter = 0;
    my %indexes = map { $_ => $counter++ } @$elements;
    #$self->logDebug("indexes", \%indexes);
    
    return \%indexes;
}


1;
