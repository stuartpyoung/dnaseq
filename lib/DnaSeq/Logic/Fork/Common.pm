package Common;
use Moose::Role;
use Moose::Util::TypeConstraints;
use Method::Signatures::Simple;

=head2

PACKAGE		Logic::Fork::

PURPOSE

    PROVIDE UTILITY METHODS FOR THE QC CLASSES
    
=cut

use strict;
use warnings;

method getSubroutine ($key) {
    my @words = split " ", $key;
    foreach my $word ( @words ) {
        $word = uc(substr($word, 0, 1)) . substr($word, 1);
    }
    $self->logDebug("words", \@words);
    
    return "check" . join "", @words;
}

method getMetrics ($parameterfile) {
    $self->logDebug("parameterfile", $parameterfile);
    my $contents	=	$self->getFileContents($parameterfile);
    my $lines       =   [   split "\n", $contents ];
    shift @$lines;
    
    my $metrics = {};
    foreach my $line ( @$lines ) {
        next if $line =~ /^\s*#/;
        my @elements = split "\t", $line;
        #$self->logDebug("elements", \@elements);
        my $key = shift @elements;
        if ( not exists $metrics->{$key} ) {
            $metrics->{$key} = [];
        }
        push @{$metrics->{$key}}, \@elements;
    }
    $self->logDebug("metrics", $metrics);
    
    return $metrics;
}

#method getBlocks ($datafile, $metrics) {
#    $self->logDebug("datafile", $datafile);
#    $self->logDebug("metrics", $metrics);
#    
#    my $blocks = {};
#    my $contents	=	$self->getFileContents($datafile);
#    my $textblocks   =   [ split ">>END_MODULE", $contents ];
#    #shift @$textblocks;
#    $self->logDebug("textblocks", $textblocks);
#    foreach my $block ( @$textblocks ) {
#        $block =~ />>([^\n]+)\s+(\S+)\n(.+)\n$/ms;
#        my $key = $1;
#        my $text = $3;
#        #$self->logDebug("key", $key);
#        #$self->logDebug("text", $text);
#        
#        if ( exists $metrics->{$key} ) {
#           $blocks->{$key} = $text;
#        }
#    }
#    
#    return $blocks;
#}

method checkColumnRules ($columnindexes, $block, $rules, $sub, $metric) {
    $self->logDebug("columnindexes", $columnindexes);
    $self->logDebug("rules", $rules);
    $self->logDebug("block", $block);
    $self->logDebug("metric", $metric);
    
    my $index = 0;
    my $lines = [ split "\n", $block ];
    shift @$lines;

    my $results =   [];
    foreach my $rule ( @$rules ) {
        $self->logDebug("rule", $rule);
        my $outcome     =   $$rule[0];
        my $rulename    =   $$rule[1];
        my $threshold   =   $$rule[2];
        my $operator    =   undef;
        $operator = $$rule[3] if defined $$rule[3];
        $self->logDebug("rulename", $rulename);
        $self->logDebug("outcome", $outcome);
        $self->logDebug("threshold", $threshold);
        
        #### GET COLUMN INDEX FOR RULE
        my $columnindex = $columnindexes->{$rulename};
        $self->logDebug("columnindex", $columnindex);
        
        #### VERIFY VALUES ARE GREATER THAN THRESHOLDS
        foreach my $line ( @$lines ) {
            my $elements=   [ split "\t", $line ];
            my $actual = $$elements[$columnindex];
            $self->logDebug("actual", $actual);
    
            my $result;
            ($index, $result) = &$sub($threshold, $actual, $outcome, $index, $operator);
            $self->logDebug("index", $index);
            $self->logDebug("result", $result);
            push @$results, {
                index   =>  $index,
                result  =>  {
                    metric      =>  $metric,
                    rulename    =>  $rulename,
                    outcome     =>  $outcome,
                    threshold   =>  $threshold,
                    actual      =>  $actual,
                    result      =>  $result
                }
            };
        }
    }
    $self->logDebug("index", $index);
    $self->logDebug("results", $results);
    $self->logDebug("*******");
    
    return $index, $results;
}

method getColumnIndexes ($datafile) {

    #### GET RULE TARGET COLUMN INDEX
    my $contents = $self->getFileContents($datafile);
    my $lines = [ split "\n", $contents ];
    my $header  =   shift @$lines;
    
    return $self->parseColumnHeader($header);
}

method parseColumnHeader ($header) {
    my $elements=   [ split "\t", $header ];
    $self->logDebug("elements", $elements);
    my $counter = 0;
    my %indexes = map { $_ => $counter++ } @$elements;
    $self->logDebug("indexes", \%indexes);

    return \%indexes;
}


1;
