package TsTv;

#### TEST

use Moose::Role;
use Moose::Util::TypeConstraints;
use Method::Signatures::Simple;

with 'Common';

=head2

PACKAGE Logic::Fork::TsTv

PURPOSE

    GENERATE TsTv RATIO
     
=cut
use strict;
use warnings;

method selectIndex ($inputfiles) {
    $self->logDebug("inputfiles", $inputfiles);

    my $files = [ split ",", $inputfiles ];
    $self->logDebug("files", $files);
    my $datafile = $$files[0];
    my $parameterfile = $$files[1];
    
    my $metrics = $self->getMetrics($parameterfile);
    $self->logDebug("metrics", $metrics);

    my $actual = $self->getActual($datafile);
    $self->logDebug("actual", $actual);
    my $index = 0;
    my $results = [];
    foreach my $key ( keys %$metrics ) {
        my $rules = $metrics->{$key};
        foreach my $rule ( @$rules ) {
            $self->logDebug("rule", $rule);
     
            my $rulename    =   $$rule[0];
            my $outcome     =   $$rule[1];
            my $threshold   =   $$rule[2];
			my $operator	=	$$rule[3];
            $self->logDebug("rulename", $rulename);
            $self->logDebug("outcome", $outcome);
            $self->logDebug("threshold", $threshold);     

            my $result = "pass";
			if ( $operator eq "max" ) {
				if ( $actual > $threshold ) {
                    $self->logDebug("fail max");
                    $result = "fail";
					if ( $outcome eq "warning" ) {
						$index = 1 if $index < 1;
					}
					elsif ( $outcome eq "failure" ) {
						$index = 2;
					}
				}
			}
			elsif ( $operator eq "min" ) {
				if ( $actual < $threshold ) {
                    $self->logDebug("fail min");
                    $result = "fail";
					if ( $outcome eq "warning" ) {
						$index = 1 if $index < 1;
					}
					elsif ( $outcome eq "failure" ) {
						$index = 2;
					}
				}				
			}
            
            push @$results, {
                index   =>  $index,
                result  =>  {
                    metric      =>  "TsTv",
                    rulename    =>  $rulename,
                    outcome     =>  $outcome,
                    threshold   =>  $threshold,
                    operator    =>  $operator,
                    actual      =>  $actual,
                    result      =>  $result
                }
            };

        }
    }

    return $index, $results;
}

method getActual ($datafile) {
    #$self->logDebug("datafile", $datafile);
    my $contents = $self->getFileContents($datafile);
    $self->logDebug("contents", $contents);

    my ($ts) = $contents =~ /Ts\s+(\S+)/;
    my ($tv) = $contents =~ /Tv\s+(\S+)/;
    #$self->logDebug("ts", $ts);
    #$self->logDebug("tv", $tv);
	
	my $actual = $ts/$tv;
    #$self->logDebug("actual", $actual);
    
    return $actual;    
}


1;   