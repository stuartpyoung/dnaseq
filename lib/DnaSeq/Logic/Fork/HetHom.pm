package HetHom;

#### TEST

use Moose::Role;
use Moose::Util::TypeConstraints;
use Method::Signatures::Simple;

with 'Common';

=head2

PACKAGE Logic::Fork::HetHom

PURPOSE

    GENERATE het/hom RATIO
    
=cut
use strict;
use warnings;

method selectIndex ($inputfiles) {
    $self->logDebug("inputfiles", $inputfiles);

    my $files = [ split ",", $inputfiles ];
    $self->logDebug("files", $files);
    my $datafile = $$files[0];
    my $parameterfile = $$files[1];
    
    my $metrics = $self->getMetrics($parameterfile);
    $self->logDebug("metrics", $metrics);
    
    my $actual = $self->getActual($datafile);    
    my $index = 0;
    my $results = [];
    foreach my $key ( keys %$metrics ) {
        my $rules = $metrics->{$key};
        foreach my $rule ( @$rules ) {
            $self->logDebug("rule", $rule);
     
            my $outcome     =   $$rule[0];
            my $rulename    =   $$rule[1];
            my $threshold   =   $$rule[2];
            $self->logDebug("rulename", $rulename);
            $self->logDebug("outcome", $outcome);
            $self->logDebug("threshold", $threshold);     
            
            my $result = "pass";
            if ( $actual > $threshold ) {
                $result = "fail";
                if ( $outcome eq "warning" ) {
                    $index = 1 if $index < 1;
                }
                elsif ( $outcome eq "failure" ) {
                    $index = 2;
                }
            }
            
            push @$results, {
                index   =>  $index,
                result  =>  {
                    metric      =>  "HetHom",
                    rulename    =>  $rulename,
                    outcome     =>  $outcome,
                    threshold   =>  $threshold,
                    actual      =>  $actual,
                    result      =>  $result
                }
            };
        }
    }
    
    return $index, $results;
}

method getActual ($datafile) {
    $self->logDebug("datafile", $datafile);
    my $contents = $self->getFileContents($datafile);
    $self->logDebug("contents", $contents);
    my ($actual) = $contents =~ /(\S+)\s*$/;
    $self->logDebug("actual", $actual);
    
    return $actual;    
}


1;   