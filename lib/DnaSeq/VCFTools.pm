use MooseX::Declare;
use Getopt::Simple;

class DnaSeq::VCFTools with Util::Logger {

use Data::Dumper;

#### Ints
has 'log'		=> ( isa => 'Int', is => 'rw', default 	=> 	0 	);has 'printlog'	=> ( isa => 'Int', is => 'rw', default 	=> 	0 	);

#### Strings
has 'logfile'	=> ( isa => 'Str|Undef', is => 'rw', required	=>	0	);

#### Objects
has 'conf'			=> 	( isa => 'Conf::Yaml', is => 'rw', lazy => 1, builder => "setConf" );

#######//////}}}}}    
    
method BUILD ($args) { 
	$self->logDebug("");    
	$self->initialise();
}

method initialise {
	$self->logCaller("");
}

method compare ($options) {

=doc

=head2 	SUBROUTINE	compare

=head2	PURPOSE		Run vcf-compare on a group of samples

=head2	INPUTS

  options HashRef: Contains the following items:

	vcftools String		: Path to VCFTools 'bin' directory
    names ArrayRef		: List of names e.g., [ "a", "b", "c",...] with
	    corresponding zipped VCF files [ "a.vcf.gz", "b.vcf.gz", ...] 
	inputdir String		: Location of zipped VCF files
	outputdir String	: Location to print output file "all.compare.tsv"	

=cut	

	my $vcftools = $options->{vcftools};
	$vcftools = $self->getVcftools() if not defined $vcftools;
	my $names = $options->{names};
	my $inputdir = $options->{inputdir};
	my $outputdir = $options->{outputdir};
	$self->logDebug("vcftools", $vcftools);
	$self->logDebug("names", $names);
	$self->logDebug("inputdir", $inputdir);
	$self->logDebug("outputdir", $outputdir);

	#### GET PAIRS
	my $pairs = $self->nonRedundantPairs($names);

	### GENERATE STATS
	$self->generateCompareStats($vcftools, $pairs, $inputdir, $outputdir);
	
	#### COLLECT STATS	
	my $stats = $self->collectCompareStats($outputdir, $names);
	#$self->logDebug("stats", $stats);
	
	#### PRINT STATS
	$self->printCompareStats($names, $stats, "$outputdir/vcf-compare.tsv");	
}

method generateCompareStats ($vcftools, $pairs, $inputdir, $outputdir) {
	$self->logDebug("count pairs", scalar(@$pairs));
	
	#### RUN vcf-compare FOR ALL PAIRS
	foreach my $pair ( @$pairs) {
		my $name1 = $$pair[0];
		my $name2 = $$pair[1];
		$self->vcfCompare($vcftools, $name1, $name2, $inputdir, $outputdir);
	}
}

method nonRedundantPairs($array) {
	$self->logDebug("array", $array);

	my $pairs	=	[];
	my $start	=	1;
	for (my $i = 0; $i < @$array; $i++ ) {
		my $name1 = $$array[$i];
		for ( my $k = $start; $k < @$array; $k++) {
			my $name2	=	$$array[$k];
			push(@$pairs, [$name1, $name2]);
		}
		$start++;
	}

	return $pairs;	
}

method vcfCompare ($vcftools, $stub1, $stub2, $inputdir, $outputdir) {
	#$self->logDebug("stub1", $stub1);
	#$self->logDebug("stub2", $stub2);
	my $vcf1 = "$inputdir/$stub1.vcf.gz";
	my $vcf2 = "$inputdir/$stub2.vcf.gz";

	my $command	=	"$vcftools/bin/vcf-compare $vcf1 $vcf2 > $outputdir/$stub1-v-$stub2.vcf-compare";
	$self->logDebug("command", $command);

	`$command`
}

method collectCompareStats($outputdir, $names) {
	$self->logDebug("outputdir", $outputdir);
	$self->logDebug("names", $names);
	
	my $pairs	=	$self->nonRedundantPairs($names);
	$self->logDebug("pairs", $pairs);
	$self->logDebug("count pairs", scalar(@$pairs));
	#foreach my $name (
	
	my $stats	=	[];
	foreach my $pair ( @$pairs ) {
		my $name1 = $$pair[0];
		my $name2 = $$pair[1];

		my $statsfile	=	"$outputdir/$name1-v-$name2.vcf-compare";
		my $stat		=	$self->parseCompareFile($statsfile);
		push(@$stats, $stat);
	}

	return $stats;	
}

method printCompareStats($names, $stats, $outputfile) {
	$self->logDebug("stats", $stats);
	$self->logDebug("count stats", scalar(@$stats));
	$self->logDebug("outputfile", $outputfile);
	
	my $twoarray = [];
	for my $i (0.. @$names - 1) {
		$$twoarray[$i][$i] = 100;
	}
	my $start = 0;
	my $counter = 0;
	my $indices;
	%$indices = map{$_ => $counter++} @$names;
	for (my $i = 0; $i < @$stats; $i++) {
		my $name1		=	$$stats[$i][0];
		my $name2		=	$$stats[$i][1];
		my $percent1	=	$$stats[$i][2];
		my $percent2	=	$$stats[$i][3];
		$$twoarray[$indices->{$name1}][$indices->{$name2}] = $percent1;
		$$twoarray[$indices->{$name2}][$indices->{$name1}] = $percent2;
	}

	my $output		=	qq{Variant calls comparison using vcf-compare\n};
	my $namestring	=	join "\t", @$names;
	$output			.=	 "\t$namestring\n";
	
	for (my $i = 0; $i < @$twoarray; $i++) {
		$output .= $$names[$i] . "\t";
		$output .= join "\t", @{$$twoarray[$i]};
		$output .= "\n";
	}
	$self->logDebug("output", $output);
	
	$self->printToFile($outputfile, $output);
}

method addEntry ($array, $position, $entry) {
	$array = [] if not defined $array;
	$$array[$position] = $entry;
	
	return $array;
}

method parseCompareFile($file) {
	return undef if not -f $file;
	my $contents	=	$self->fileContents($file);
	$contents	=~ 	s/^.+?\nVN\s/VN /ms;
	$contents	=~ 	s/^#SN.+$//ms;
	$self->logDebug("contents", "\n\n$contents\n");
	
	my $combinations;
	@$combinations	=	split "\n", $contents;
	foreach my $combination (@$combinations) {
		if ($combination =~ /^VN\s+\d+\s+(\/\S+?\/)?([^\/]+)\.vcf\.gz\s+(\S+)\s+(\/\S+?\/)?([^\/]+)\.vcf\.gz\s+(\S+)/ ) {
			my $name1		=	$2;
			my $percent1	=	$3;
			my $name2		=	$5;
			my $percent2	=	$6;
			$self->logDebug("name1", $name1);
			$self->logDebug("name2", $name2);
			$percent1	=~ s/[\(\)%]//g;
			$percent2	=~ s/[\(\)%]//g;
			$self->logDebug("percent1", $percent1);
			$self->logDebug("percent2", $percent2);
		
			return [$name1, $name2, $percent1, $percent2];
		}
	}

	return undef;	
}

method getVcftools {
	my $version = $self->latestVersion("vcftools");
	$self->logDebug("version", $version);

	return $self->getInstalldir("vcftools", $version)
}

method tabixIndex ($options) {
	$self->logDebug("options", $options);
	
	my $tabix = $options->{tabix};
	$tabix = $self->getTabix() if not defined $tabix;
	my $names = $options->{names};
	my $inputdir = $options->{inputdir};
	my $outputdir = $options->{outputdir};
	$self->logDebug("tabix", $tabix);
	$self->logDebug("names", $names);
	$self->logDebug("inputdir", $inputdir);
	$self->logDebug("outputdir", $outputdir);
	
	#### RUN bgzip AND tabix FOR ALL PAIRS
	foreach my $name ( @$names ) {
		my $command = "$tabix/bgzip $inputdir/$name.vcf";
		$self->logDebug("command", $command);
		`$command`
	}

	#### RUN bgzip AND tabix FOR ALL PAIRS
	foreach my $name ( @$names ) {
		my $command = "$tabix/tabix -s 1 -b 2 -e 3 $inputdir/$name.vcf.gz";
		$self->logDebug("command", $command);
		`$command`
	}
}

method getTabix {
	my $version = $self->latestVersion("tabix");
	$self->logDebug("version", $version);

	return $self->getInstalldir("tabix", $version)
}

method getVcfStub ($vcf) {
	$self->logDebug("vcf", $vcf);
	
	my ($stub) = $vcf =~ /^(.+?)\.vcf(\.gz)?/;

	return $stub;	
}

method getInstalldir ($package, $version) {
	my $entry	=	$self->conf()->getKey("packages", $package);
	$self->logDebug("entry", $entry);
	
	return $entry->{$version}->{INSTALLDIR};
}

method latestVersion ($package) {
	$self->logDebug("package", $package);
	my $subkey	=	undef;
	my $installations	=	$self->conf()->getKey("packages", $package);
	$self->logDebug("installations", $installations);

	my $versions;
	@$versions	=	keys %$installations;
	$self->logDebug("versions", $versions);

	#### ASSUMES KEYS ARE SORTED
	my $latest	=	$$versions[ scalar(@$versions) - 1];
	$self->logDebug("latest", $latest);
	
	return $latest;
}

method fileContents ($filename) {
	$self->logNote("filename", $filename);
	open(FILE, $filename) or die "Can't open filename: $filename\n";
	my $oldsep = $/;
	$/ = undef;
	my $contents = <FILE>;
	close(FILE);
	$/ = $oldsep;
	
	return $contents;
}

method printToFile ($file, $text) {
    open(FILE, ">$file") or die "Can't open file: $file\n";
    print FILE $text;    
    close(FILE) or die "Can't close file: $file\n";
}

#__PACKAGE__->meta->make_immutable( inline_constructor => 0 );

}


