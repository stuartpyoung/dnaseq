---
apps:
  -
    appname: installZip
    appnumber: 1
    description: 'Update biorepository'
    installdir: /usr/bin
    location: apt-get
    owner: agua
    package: dnaseq
    parameters:
      -
        description: Subcommand
        discretion: required
        ordinal: 1
        paramname: subcommand
        value: install
      -
        argument: -y
        description: 'Respond yes to all prompts'
        discretion: optional
        ordinal: 2
        paramname: yesoption
      -
        description: target
        discretion: required
        ordinal: 3
        paramname: target
        value: zip
    apptype: install
    url: www.github.com/agua/dnaseq
    version: latest
  -
    appname: unzipFastQC
    appnumber: 2
    description: 'Unzip a file with zip'
    installdir: /usr/bin
    location: unzip
    owner: agua
    package: dnaseq
    parameters:
      -
        argument: -f
        description: 'Freshen existing files (i.e., overwrite without prompting)'
        discretion: optional
        paramname: freshen
        valuetype: flag
      -
        argument: -d
        description: 'Target directory'
        discretion: optional
        paramname: targetdir
        value: <USERHOME>/data/bam/<SAMPLE>
      -
        description: 'Target file'
        discretion: optional
        paramname: targetfile
        value: <USERHOME>/data/bam/<SAMPLE>/<SAMPLE>_fastqc.zip
    apptype: qc
    url: www.github.com/agua/dnaseq
    version: 0.0.1
  -
    appname: forkfastqc
    appnumber: 3
    description: 'Fork workflow based on FastQC output'
    installdir: <INSTALLDIR>
    location: bin/logic/fork
    owner: agua
    package: dnaseq
    parameters:
      -
        argument: --modfile
        description: 'Location of module file'
        discretion: optional
        ordinal: '1'
        paramname: modfile
        value: <INSTALLDIR>/lib/Logic/Fork/FastQC.pm
        valuetype: file
      -
        argument: --inputfiles
        description: 'Comma separated list of input files'
        discretion: optional
        ordinal: '2'
        paramname: inputfiles
        value: <USERHOME>/data/bam/<SAMPLE>/<SAMPLE>_fastqc/fastqc_data.txt,<INSTALLDIR>/data/tsv/forkfastqc/parameters.tsv
        valuetype: string
      -
        argument: --if
        description: 'Stage number of IF path'
        discretion: required
        ordinal: '2'
        paramname: if
        value: '2'
        valuetype: integer
      -
        argument: --else
        description: 'Stage number of ELSE path'
        discretion: required
        ordinal: '3'
        paramname: else
        value: '3'
        valuetype: integer
    apptype: qc
    url: www.github.com/agua/dnaseq
    version: 0.0.1
  -
    appname: alertfastqc
    appnumber: 4
    description: 'Send email to recipient with specified subject and message body'
    installdir: <INSTALLDIR>
    location: bin/logic/email
    owner: agua
    package: dnaseq
    parameters:
      -
        argument: --to
        description: 'Recipient email address (e.g., stuart.young@inova.com)'
        discretion: required
        paramname: to
        value: stuart.young@inova.com
        valuetype: string
      -
        argument: --from
        description: 'Displayed name of sender'
        discretion: required
        paramname: from
        value: aguanoreply@gmail.com
        valuetype: string
      -
        argument: --subject
        description: 'Subject/title of email'
        discretion: required
        paramname: subject
        value: 'Sample %SAMPLE% failed to pass FastQC [%USERNAME%:%PROJECT%:%WORKFLOW%|continue:%STAGENUMBER%]'
        valuetype: string
      -
        argument: --message
        description: 'Message body/content of email'
        discretion: required
        paramname: message
        value: "Processing of sample %SAMPLE% in workflow "%PROJECT%:%WORKFLOW%" stopped at stage %STAGENUMBER%: %STAGE%.\nCheck the following files to troubleshoot.\nSTDOUT    /home/%USERNAME%/agua/%PROJECT%/%WORKFLOW%/%STAGE%/stdout/%SAMPLE%.*.stdout\nSTDERR    /home/%USERNAME%/agua/%PROJECT%/%WORKFLOW%/%STAGE%/stdout/%SAMPLE%.*.stderr\nReply with \"Continue\" as the first line of your message to continue processing sample %SAMPLE% from the next stage in the workflow\n"
        valuetype: string
      -
        argument: --project
        description: 'Project name. Will source from PROJECT environment variable if not provided'
        discretion: optional
        paramname: project
        value: '%PROJECT%'
        valuetype: string
      -
        argument: --workflow
        description: 'Project name. Will source from WORKFLOW environment variable if not provided'
        discretion: optional
        paramname: workflow
        value: '%WORKFLOW%'
        valuetype: string
      -
        argument: --stagenumber
        description: 'Stage number. Will source from STAGENUMBER environment variable if not provided'
        discretion: optional
        paramname: stagenumber
        value: '%STAGENUMBER%'
        valuetype: string
      -
        argument: --stage
        description: 'Stage name. Will source from STAGE environment variable if not provided'
        discretion: optional
        paramname: stage
        value: '%STAGE%'
        valuetype: string
      -
        argument: --sample
        description: 'Sample ID. Will source from SAMPLE environment variable if not provided'
        discretion: optional
        paramname: sample
        value: '%SAMPLE%'
        valuetype: string
    apptype: qc
    url: www.github.com/agua/dnaseq
    version: 0.0.1
  -
    appname: forkhethom
    appnumber: 5
    description: 'Fork workflow based on het/hom output'
    installdir: <INSTALLDIR>
    location: bin/logic/fork
    owner: agua
    package: dnaseq
    parameters:
      -
        argument: --modfile
        description: 'Location of module file'
        discretion: optional
        ordinal: '1'
        paramname: modfile
        value: <INSTALLDIR>/lib/Logic/Fork/HetHom.pm
        valuetype: file
      -
        argument: --inputfiles
        description: 'Comma separated list of input files'
        discretion: optional
        ordinal: '2'
        paramname: inputfiles
        value: <USERHOME>/data/bam/<SAMPLE>/<SAMPLE>.hethom,<INSTALLDIR>/data/tsv/forkhethom/parameters.tsv
        valuetype: string
      -
        argument: --if
        description: 'Stage number of IF path'
        discretion: required
        ordinal: '2'
        paramname: if
        value: '4'
        valuetype: integer
      -
        argument: --else
        description: 'Stage number of ELSE path'
        discretion: required
        ordinal: '3'
        paramname: else
        value: '5'
        valuetype: integer
    apptype: qc
    url: www.github.com/agua/dnaseq
    version: 0.0.1
  -
    appname: alerthethom
    appnumber: 6
    description: 'Send email to recipient with specified subject and message body'
    installdir: <INSTALLDIR>
    location: bin/logic/email
    owner: agua
    package: dnaseq
    parameters:
      -
        argument: --to
        description: 'Recipient email address (e.g., stuart.young@inova.com)'
        discretion: required
        paramname: to
        value: stuart.young@inova.com
        valuetype: string
      -
        argument: --from
        description: 'Displayed name of sender'
        discretion: required
        paramname: from
        value: aguanoreply@gmail.com
        valuetype: string
      -
        argument: --subject
        description: 'Subject/title of email'
        discretion: required
        paramname: subject
        value: 'Sample %SAMPLE% failed to pass FastQC [%USERNAME%:%PROJECT%:%WORKFLOW%|continue:%STAGENUMBER%]'
        valuetype: string
      -
        argument: --message
        description: 'Message body/content of email'
        discretion: required
        paramname: message
        value: "Processing of sample %SAMPLE% in workflow "%PROJECT%:%WORKFLOW%" stopped at stage %STAGENUMBER%: %STAGE%.\nCheck the following files to troubleshoot.\nSTDOUT    /home/%USERNAME%/agua/%PROJECT%/%WORKFLOW%/%STAGE%/stdout/%SAMPLE%.*.stdout\nSTDERR    /home/%USERNAME%/agua/%PROJECT%/%WORKFLOW%/%STAGE%/stdout/%SAMPLE%.*.stderr\nReply with \"Continue\" as the first line of your message to continue processing sample %SAMPLE% from the next stage in the workflow\n"
        valuetype: string
      -
        argument: --project
        description: 'Project name. Will source from PROJECT environment variable if not provided'
        discretion: optional
        paramname: project
        value: '%PROJECT%'
        valuetype: string
      -
        argument: --workflow
        description: 'Project name. Will source from WORKFLOW environment variable if not provided'
        discretion: optional
        paramname: workflow
        value: '%WORKFLOW%'
        valuetype: string
      -
        argument: --stagenumber
        description: 'Stage number. Will source from STAGENUMBER environment variable if not provided'
        discretion: optional
        paramname: stagenumber
        value: '%STAGENUMBER%'
        valuetype: string
      -
        argument: --stage
        description: 'Stage name. Will source from STAGE environment variable if not provided'
        discretion: optional
        paramname: stage
        value: '%STAGE%'
        valuetype: string
      -
        argument: --sample
        description: 'Sample ID. Will source from SAMPLE environment variable if not provided'
        discretion: optional
        paramname: sample
        value: '%SAMPLE%'
        valuetype: string
    apptype: qc
    url: www.github.com/agua/dnaseq
    version: 0.0.1
  -
    appname: forktstv
    appnumber: 7
    description: 'Fork workflow based on ts/tv output'
    installdir: <INSTALLDIR>
    location: bin/logic/fork
    owner: agua
    package: dnaseq
    parameters:
      -
        argument: --modfile
        description: 'Location of module file'
        discretion: optional
        ordinal: '1'
        paramname: modfile
        value: <INSTALLDIR>/lib/Logic/Fork/TsTv.pm
        valuetype: file
      -
        argument: --inputfiles
        description: 'Comma separated list of input files'
        discretion: optional
        ordinal: '2'
        paramname: inputfiles
        value: <USERHOME>/data/bam/<SAMPLE>/<SAMPLE>.tstv,<INSTALLDIR>/data/tsv/forktstv/parameters.tsv
        valuetype: string
      -
        argument: --if
        description: 'Stage number of IF path'
        discretion: required
        ordinal: '2'
        paramname: if
        value: '6'
        valuetype: integer
      -
        argument: --else
        description: 'Stage number of ELSE path'
        discretion: required
        ordinal: '3'
        paramname: else
        value: '7'
        valuetype: integer
    apptype: qc
    url: www.github.com/agua/dnaseq
    version: 0.0.1
  -
    appname: alerttstv
    appnumber: 8
    description: 'Send email to recipient with specified subject and message body'
    installdir: <INSTALLDIR>
    location: bin/logic/email
    owner: agua
    package: dnaseq
    parameters:
      -
        argument: --to
        description: 'Recipient email address (e.g., stuart.young@inova.com)'
        discretion: required
        paramname: to
        value: stuart.young@inova.com
        valuetype: string
      -
        argument: --from
        description: 'Displayed name of sender'
        discretion: required
        paramname: from
        value: aguanoreply@gmail.com
        valuetype: string
      -
        argument: --subject
        description: 'Subject/title of email'
        discretion: required
        paramname: subject
        value: 'Sample %SAMPLE% failed to pass FastQC [%USERNAME%:%PROJECT%:%WORKFLOW%|continue:%STAGENUMBER%]'
        valuetype: string
      -
        argument: --message
        description: 'Message body/content of email'
        discretion: required
        paramname: message
        value: "Processing of sample %SAMPLE% in workflow "%PROJECT%:%WORKFLOW%" stopped at stage %STAGENUMBER%: %STAGE%.\nCheck the following files to troubleshoot.\nSTDOUT    /home/%USERNAME%/agua/%PROJECT%/%WORKFLOW%/%STAGE%/stdout/%SAMPLE%.*.stdout\nSTDERR    /home/%USERNAME%/agua/%PROJECT%/%WORKFLOW%/%STAGE%/stdout/%SAMPLE%.*.stderr\nReply with \"Continue\" as the first line of your message to continue processing sample %SAMPLE% from the next stage in the workflow\n"
        valuetype: string
      -
        argument: --project
        description: 'Project name. Will source from PROJECT environment variable if not provided'
        discretion: optional
        paramname: project
        value: '%PROJECT%'
        valuetype: string
      -
        argument: --workflow
        description: 'Project name. Will source from WORKFLOW environment variable if not provided'
        discretion: optional
        paramname: workflow
        value: '%WORKFLOW%'
        valuetype: string
      -
        argument: --stagenumber
        description: 'Stage number. Will source from STAGENUMBER environment variable if not provided'
        discretion: optional
        paramname: stagenumber
        value: '%STAGENUMBER%'
        valuetype: string
      -
        argument: --stage
        description: 'Stage name. Will source from STAGE environment variable if not provided'
        discretion: optional
        paramname: stage
        value: '%STAGE%'
        valuetype: string
      -
        argument: --sample
        description: 'Sample ID. Will source from SAMPLE environment variable if not provided'
        discretion: optional
        paramname: sample
        value: '%SAMPLE%'
        valuetype: string
    apptype: qc
    url: www.github.com/agua/dnaseq
    version: 0.0.1
workflowname: ClinQCFork
