#!/bin/bash

echo 'Running a single stage of a workflow'

# 1. Run FixMate stage of workflow FixMates
flow runStage --project NA12878 --workflow FixMates --username admin --log 4 --stagenumber 2  --samplestring "sample:NA12878"

#### 2. Run realignTarget stage of workflow IndelRealignment 
###flow runStage --project runSRA --workflow IndelRealignment --username admin --log 4 --stagenumber 1  --samplestring "sample:NA12878"
###
#### 3. Run realign stage of workflow IndelRealignment 
###flow runStage --project runSRA --workflow IndelRealignment --username admin --log 4 --stagenumber 2  --samplestring "sample:NA12878"
###

