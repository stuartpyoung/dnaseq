#!/bin/bash

# 1. Load the workflows

cd $INSTALLDIR/conf/run
flow deleteproject NA12878 --username admin
flow addproject NA12878 --username admin
flow addworkflow NA12878 1-FixMates.work --username admin
flow addworkflow NA12878 2-FilterReads.work --username admin
flow addworkflow NA12878 3-MarkDuplicates.work --username admin
flow addworkflow NA12878 4-AddReadGroups.work --username admin
flow addworkflow NA12878 5-QualityFilter.work --username admin
flow addworkflow NA12878 6-IndelRealignment.work --username admin
flow addworkflow NA12878 7-BaseRecalibration.work --username admin
flow addworkflow NA12878 8-HaplotypeCaller.work --username admin
flow addworkflow NA12878 9-Varscan.work --username admin
flow addworkflow NA12878 10-FreeBayes.work --username admin
