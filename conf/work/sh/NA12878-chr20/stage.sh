#!/bin/bash

echo 'Running a single stage of a workflow'

# 1. Run FixMate stage of workflow FixMates
flow runStage --project NA12878-chr20 --workflow FixMates --username admin --log 4 --stagenumber 2  --samplestring "sample:NA12878-chr20"

#### 2. Run realignTarget stage of workflow IndelRealignment 
###flow runStage --project NA12878-chr20 --workflow IndelRealignment --username admin --log 4 --stagenumber 1  --samplestring "sample:NA12878-chr20"
###
#### 3. Run realign stage of workflow IndelRealignment 
###flow runStage --project NA12878-chr20 --workflow IndelRealignment --username admin --log 4 --stagenumber 2  --samplestring "sample:NA12878-chr20"
###

