#!/bin/bash

echo 'Running the workflows consecutively for project NA12878-chr20'

# 1. FixMates
#flow runworkflow NA12878-chr20 FixMates --username admin --log 4 --samplestring "sample:NA12878-chr20"

# 2. FilterReads
#flow runworkflow NA12878-chr20 FilterReads --username admin --log 4 --samplestring "sample:NA12878-chr20"

# 3. MarkDuplicates
#flow runworkflow NA12878-chr20 MarkDuplicates --username admin --log 4 --samplestring "sample:NA12878-chr20"		

# 4. AddReadGroups
#flow runworkflow NA12878-chr20 AddReadGroups --username admin --log 4 --samplestring "sample:NA12878-chr20"
	
# 5. QualityFilter
#flow runworkflow NA12878-chr20 QualityFilter --username admin --log 4 --samplestring "sample:NA12878-chr20"

# 6. IndelRealignment
flow runworkflow NA12878-chr20 IndelRealignment --username admin --log 4 --samplestring "sample:NA12878-chr20"

# 7. BaseRecalibration
flow runworkflow NA12878-chr20 BaseRecalibration --username admin --log 4 --samplestring "sample:NA12878-chr20"

# 8. HaplotypeCaller
flow runworkflow NA12878-chr20 HaplotypeCaller --username admin --log 4 --samplestring "sample:NA12878-chr20"

