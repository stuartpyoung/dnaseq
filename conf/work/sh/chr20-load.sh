#!/bin/bash

# 1. Load the workflows

cd $INSTALLDIR/conf/run
flow deleteproject NA12878-chr20 --username admin
flow addproject NA12878-chr20 --username admin
flow addworkflow NA12878-chr20 1-FixMates.work --username admin
flow addworkflow NA12878-chr20 2-FilterReads.work --username admin
flow addworkflow NA12878-chr20 3-MarkDuplicates.work --username admin
flow addworkflow NA12878-chr20 4-AddReadGroups.work --username admin
flow addworkflow NA12878-chr20 5-QualityFilter.work --username admin
flow addworkflow NA12878-chr20 6-IndelRealignment.work --username admin
flow addworkflow NA12878-chr20 7-BaseRecalibration.work --username admin
flow addworkflow NA12878-chr20 8-HaplotypeCaller.work --username admin
flow addworkflow NA12878-chr20 9-Varscan.work --username admin
flow addworkflow NA12878-chr20 10-FreeBayes.work --username admin
