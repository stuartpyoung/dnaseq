#!/bin/bash

echo 'Running the workflows consecutively for project NA12878'

# 1. FixMates
flow runworkflow NA12878 FixMates --username admin --log 4 --samplestring "sample:NA12878"

# 2. FilterReads
flow runworkflow NA12878 FilterReads --username admin --log 4 --samplestring "sample:NA12878"

# 3. MarkDuplicates
flow runworkflow NA12878 MarkDuplicates --username admin --log 4 --samplestring "sample:NA12878"		

# 4. AddReadGroups
flow runworkflow NA12878 AddReadGroups --username admin --log 4 --samplestring "sample:NA12878"
	
# 5. QualityFilter
flow runworkflow NA12878 QualityFilter --username admin --log 4 --samplestring "sample:NA12878"

# 6. IndelRealignment
flow runworkflow NA12878 IndelRealignment --username admin --log 4 --samplestring "sample:NA12878"

# 7. BaseRecalibration
flow runworkflow NA12878 BaseRecalibration --username admin --log 4 --samplestring "sample:NA12878"

# 8. HaplotypeCaller
flow runworkflow NA12878 HaplotypeCaller --username admin --log 4 --samplestring "sample:NA12878"

