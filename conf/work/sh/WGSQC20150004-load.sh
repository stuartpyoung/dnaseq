#!/bin/bash

# 1. Load the workflows

cd $INSTALLDIR/conf/run
flow deleteproject WGSQC20150004 --username admin
flow addproject WGSQC20150004 --username admin
flow addworkflow WGSQC20150004 1-FixMates.work --username admin
flow addworkflow WGSQC20150004 2-FilterReads.work --username admin
flow addworkflow WGSQC20150004 3-MarkDuplicates.work --username admin
flow addworkflow WGSQC20150004 4-AddReadGroups.work --username admin
flow addworkflow WGSQC20150004 5-QualityFilter.work --username admin
flow addworkflow WGSQC20150004 6-IndelRealignment.work --username admin
flow addworkflow WGSQC20150004 7-BaseRecalibration.work --username admin
flow addworkflow WGSQC20150004 8-HaplotypeCaller.work --username admin
flow addworkflow WGSQC20150004 9-Varscan.work --username admin
flow addworkflow WGSQC20150004 10-FreeBayes.work --username admin
