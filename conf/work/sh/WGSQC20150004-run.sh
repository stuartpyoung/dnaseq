#!/bin/bash

echo 'Running the workflows consecutively for project WGSQC20150004'

# 1. FixMates
flow runworkflow WGSQC20150004 FixMates --username admin --log 4 --samplestring "sample:WGSQC20150004"

# 2. FilterReads
flow runworkflow WGSQC20150004 FilterReads --username admin --log 4 --samplestring "sample:WGSQC20150004"

# 3. MarkDuplicates
flow runworkflow WGSQC20150004 MarkDuplicates --username admin --log 4 --samplestring "sample:WGSQC20150004"		

# 4. AddReadGroups
flow runworkflow WGSQC20150004 AddReadGroups --username admin --log 4 --samplestring "sample:WGSQC20150004"
	
# 5. QualityFilter
flow runworkflow WGSQC20150004 QualityFilter --username admin --log 4 --samplestring "sample:WGSQC20150004"

# 6. IndelRealignment
flow runworkflow WGSQC20150004 IndelRealignment --username admin --log 4 --samplestring "sample:WGSQC20150004"

# 7. BaseRecalibration
flow runworkflow WGSQC20150004 BaseRecalibration --username admin --log 4 --samplestring "sample:WGSQC20150004"

# 8. HaplotypeCaller
flow runworkflow WGSQC20150004 HaplotypeCaller --username admin --log 4 --samplestring "sample:WGSQC20150004"

