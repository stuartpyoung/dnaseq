---
description: 'Mark duplicate reads'
executor: '<DEPENDENCY:java>/bin/java -jar'
installdir: <DEPENDENCY:picard>
location: MarkDuplicates.jar
appname: markDuplicates
owner: agua
packagename: dnaseq
apptype: duplicates
version: 0.1.19
parameters:
  -
    description: 'Location of input BAM file'
    discretion: required
    ordinal: 1
    paramname: inputfile
    value: I=/data/dnaseq/bam/<SAMPLE>/<SAMPLE>.fxmt_flt.bam
    valuetype: file
  -
    argument: O=
    description: 'Location of output BAM file'
    discretion: required
    ordinal: 2
    paramname: O
    value: <USERHOME>/data/dnaseq/bam/<SAMPLE>/<SAMPLE>.rmdup.bam
    valuetype: file
  -
    description: 'Location of output report file'
    discretion: optional
    argument: M=
    ordinal: 3
    paramname: M
    value: <USERHOME>/data/dnaseq/bam/<SAMPLE>/<SAMPLE>.dup_report.txt
    valuetype: file
  -
    description: 'ID for the program group record (inserted into the read group header). Set to null to disable PG record creation. Or, add a suffix to avoid collision with other program record IDs.'
    discretion: optional
    argument: PROGRAM_RECORD_ID=
    ordinal: 4
    paramname: PROGRAM_RECORD_ID
    value: 'null'
    valuetype: string
  -
    argument: VALIDATION_STRINGENCY=
    description: 'Validation stringency for all SAM files read by this program.  Set to SILENT to improve performance when processing a BAM file in which variable-length data (read, qualities, tags) do not otherwise need to be decoded.'
    discretion: optional
    ordinal: 5
    paramname: VALIDATION_STRINGENCY
    value: SILENT
    valuetype: 'LENIENT|SILENT'
  -
    argument: REMOVE_DUPLICATES=
    description: 'Set to true to automatically remove the marked duplicates'
    discretion: optional
    ordinal: 6
    paramname: REMOVE_DUPLICATES
    value: 'true'
    valuetype: 'true|false'
    
