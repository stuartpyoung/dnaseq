---
description: 'Realign indels using GATK'
executor: '<DEPENDENCY:java>/bin/java -jar'
installdir: <DEPENDENCY:gatk>
location: GenomeAnalysisTK.jar
appname: realign
owner: agua
packagename: dnaseq
version: 0.0.1
apptype: align
parameters:
  -
    argument: -T
    description: 'GATK subcommand'
    discretion: required
    ordinal: 1
    paramname: T
    value: IndelRealigner
    valuetype: string
  -
    argument: -I
    description: 'Location of input BAM file'
    discretion: essential
    ordinal: 2
    paramname: I
    value: <USERHOME>/data/dnaseq/bam/<SAMPLE>/<SAMPLE>.rmdup_grp_rmlq.bam
    valuetype: file
  -
    argument: -R
    description: 'Location of reference FASTA file'
    discretion: essential
    ordinal: 3
    paramname: R
    value: <USERHOME>/data/reference/gatk/ucsc.hg19.fasta
    valuetype: file
  -
    argument: -targetIntervals
    description: 'Location of target intervals file'
    discretion: essential
    ordinal: 4
    paramname: targetIntervals
    value: <USERHOME>/data/dnaseq/bam/<SAMPLE>/<SAMPLE>.forRealign.intervals
    valuetype: file
  -
    argument: --out
    description: 'Location of output BAM file'
    discretion: required
    ordinal: 5
    paramname: out
    value: <USERHOME>/data/dnaseq/bam/<SAMPLE>/<SAMPLE>.realigned.bam
    valuetype: file
  -
    argument: -LOD
    description: 'LOD threshold above which the cleaner will clean (default: 5.0)'
    discretion: required
    ordinal: 6
    paramname: LOD
    value: '0.4'
    valuetype: decimal
  -
    argument: -compress
    description: 'Compression level to use for writing BAM files (0 - 9, higher is more compressed)'
    discretion: required
    ordinal: 7
    paramname: compress
    value: 5
    valuetype: integer
