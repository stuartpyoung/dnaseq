---
appname: forktstv
description: 'Fork workflow based on ts/tv output'
installdir: <INSTALLDIR>
location: bin/logic/fork
owner: agua
packagename: dnaseq
version: 0.0.1
apptype: qc
parameters:
  -
    argument: --modfile
    description: 'Location of module file'
    discretion: optional
    ordinal: 1
    paramname: modfile
    value: <INSTALLDIR>/lib/Logic/Fork/tstv.pm
    valuetype: file
  -
    argument: --inputfiles
    description: 'Comma separated list of input files'
    discretion: optional
    ordinal: 2
    paramname: modfile
    value: <USERHOME>/data/dnaseq/bam/<SAMPLE>/<SAMPLE>.tstv,<INSTALLDIR>/data/tsv/forktstv/parameters.tsv
    valuetype: string
  -
    argument: --if
    description: 'Stage number of IF path'
    discretion: required
    ordinal: 2
    paramname: if
    value: 3
    valuetype: integer
  -
    argument: --else
    description: 'Stage number of ELSE path'
    discretion: required
    ordinal: 3
    paramname: else
    value: 4
    valuetype: integer
